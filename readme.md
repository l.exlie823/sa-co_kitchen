# SA 期末專案 - 第 9 組

## 建立專案

1. 匯入 sql 檔，檔案內包含建立 database 的指令

## 專案 API 文件

1. API 網址：/co_kitchen/doc
    ```
    程式撰寫期間，即搭配doc comment對各function寫註解，再透過Javadoc工具自動生成網頁API文件
    ```

## 檔案架構

1. Html 與 JS
    - 因為子系統多，命名方式相同，檔案分布於多個資料夾內(account, kitchen, rent, transaction, visa)
2. 網頁工具列
    - 因為工具列 html 相當多，且所有頁面工具列皆相同，因此獨立於 header.html，再另行使用 header.js 引入
3. AuthController
    - 原則上一張資料表對應一支 Controller，而 AuthController 處理登入登出相關功能，因此與 AccountController 相同，對應 accounts 資料表
