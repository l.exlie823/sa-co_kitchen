$(document).ready(function () {
  // 取得url上的參數
  const urlParams = new URLSearchParams(window.location.search);
  const id = urlParams.get("id");

  // 確認有要求id才進行資料查詢，否則重導至首頁
  if (id == null || id == "") {
    window.location = "/co_kitchen/index.html";
  } else {
    // 填入button href
    $("#edit").attr("href", "edit.html?id=" + id);
    $("#changepw").attr("href", "changepw.html?id=" + id);

    $.ajax({
      url: "api",
      type: "GET",
      crossDomain: true,
      cache: false,
      data: "id=" + id,
      dataType: "json",
      timeout: 5000,
      success: function (data) {
        console.log(data);

        if ("errors" in data) {
          alert(data.errors);
        } else if ("message" in data) {
          $("#id").text(data.result.data[0]["id"]);
          $("#name").text(
            data.result.data[0]["lastName"] +
              " " +
              data.result.data[0]["firstName"]
          );
          $("#email").text(data.result.data[0]["email"]);
          $("#deleted").text(data.result.data[0]["deleted"] ? "T" : "F");
        }
      },
      error: function (error) {
        console.log(error);
        alert(
          "Error code: " +
            error.status +
            "\n\nDescribtion: \n" +
            error.statusText
        );
      },
    }).then(function () {
      // 取得該帳號所有信用卡資料
      $.ajax({
        url: "/co_kitchen/visa/api",
        type: "GET",
        crossDomain: true,
        cache: false,
        data: "accountId=" + id,
        dataType: "json",
        timeout: 5000,
        success: function (data) {
          console.log(data);

          // 根據response訊息進行不同動作
          if ("errors" in data) {
            alert(data.errors);
          } else if ("message" in data) {
            // 將每一筆資料依序放入表格
            data.result.data.forEach((element) => {
              updateTable(element);
            });
          }
        },
        error: function (error) {
          console.log(error);

          // 顯示錯誤訊息
          alert(
            "Error code: " +
              error.status +
              "\n\nDescribtion: \n" +
              error.statusText
          );
        },
      }).then(function () {
        if ($("#deleted").text() == "T") {
          // 使編輯與刪除鈕失效
          $("a[id^='edit']").each(function () {
            $(this).addClass("disabled");
          });
          $("#changepw").addClass("disabled");
          $("button[id^='delete']").each(function () {
            $(this).addClass("disabled");
          });
        }
      });
    });
  }

  /**
   * 填入取得的信用卡資料
   *
   * @param element 欲填入資料
   */
  function updateTable(element) {
    if (element["deleted"] == false) {
      // 複製表格內容模板
      clone = $("#tbody-template").clone(true, true);
      // 移除隱藏用的CSS class
      clone.removeClass("d-none");

      // 設定該區塊的id
      clone.attr("id", element["id"]);
      // 填入帳號資料
      clone.find("#visaId").text(element["id"]);
      clone.find("#bankId").text(element["bankId"]);
      clone.find("#cardId").text(element["cardId"].match(/\d{4}/g).join("-"));
      let goodThru = new Date(element["goodThru"]);
      clone
        .find("#goodThru")
        .text(goodThru.getMonth() + 1 + " / " + goodThru.getFullYear());
      // 設定button的href
      clone
        .find("#information")
        .attr("href", "/co_kitchen/visa/information.html?id=" + element["id"]);
      clone
        .find("#editVisa")
        .attr("href", "/co_kitchen/visa/edit.html?id=" + element["id"]);

      // 放入最後一行
      clone.appendTo("tbody");
    }
  }

  // 確認刪除意願後進行帳號刪除
  $("#delete").click(function () {
    // 確認刪除意願，同意後進行刪除
    if (confirm("確定刪除此帳號？")) {
      // 建立要送出資料的json
      let data = {
        id: id,
      };
      // 轉成string以供後端讀取
      const dataString = JSON.stringify(data);

      $.ajax({
        url: "api",
        type: "DELETE",
        crossDomain: true,
        cache: false,
        data: dataString,
        dataType: "json",
        timeout: 5000,
        success: function (data) {
          console.log(data);

          // 根據response訊息進行不同動作
          if ("errors" in data) {
            alert(data.errors);
          } else if ("message" in data) {
            alert(data.message);

            // 刷新頁面
            window.location = "/co_kitchen";
          }
        },
        error: function (error) {
          console.log(error);

          // 顯示錯誤訊息
          alert(
            "Error code: " +
              error.status +
              "\n\nDescribtion: \n" +
              error.statusText
          );
        },
      });
    }
  });

  // 確認刪除意願後進行信用卡刪除
  $("#deleteVisa").click(function () {
    // 建立要送出資料的json
    let data = {
      id: $(this).parents("tr").attr("id"),
    };
    // 轉成string以供後端讀取
    const dataString = JSON.stringify(data);

    // 確認刪除意願，同意後進行刪除
    if (confirm("確定刪除編號" + data.id + "的信用卡？")) {
      $.ajax({
        url: "/co_kitchen/visa/api",
        type: "DELETE",
        crossDomain: true,
        cache: false,
        data: dataString,
        dataType: "json",
        timeout: 5000,
        success: function (data) {
          console.log(data);

          // 根據response訊息進行不同動作
          if ("errors" in data) {
            alert(data.errors);
          } else if ("message" in data) {
            alert(data.message);

            // 刷新頁面
            location.reload();
          }
        },
        error: function (error) {
          console.log(error);

          // 顯示錯誤訊息
          alert(
            "Error code: " +
              error.status +
              "\n\nDescribtion: \n" +
              error.statusText
          );
        },
      });
    }
  });
});
