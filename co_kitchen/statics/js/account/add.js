function loadContent(admin) {
  if (admin != true) {
    window.location = "/co_kitchen";
  }
}

$(document).ready(function () {
  // 啟用Bootstrap tooltip功能
  var tooltipTriggerList = [].slice.call(
    document.querySelectorAll('[data-bs-toggle="tooltip"]')
  );
  var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl);
  });

  // 信箱與密碼格式規則
  const emailRule = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/;
  const passwordRule = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;

  // 檢查姓氏不為空白
  $("#lastName").on("input", function () {
    if ($("#lastName").val().trim() != "") {
      $("#lastName").removeClass("is-invalid");
      $("#lastName").addClass("is-valid");
    } else {
      $("#lastName").removeClass("is-valid");
      $("#lastName").addClass("is-invalid");
    }
  });

  // 檢查名字不為空白
  $("#firstName").on("input", function () {
    if ($("#firstName").val().trim() != "") {
      $("#firstName").removeClass("is-invalid");
      $("#firstName").addClass("is-valid");
    } else {
      $("#firstName").removeClass("is-valid");
      $("#firstName").addClass("is-invalid");
    }
  });

  //檢查信箱合乎信箱規則
  $("#email").on("input", function () {
    if (emailRule.test($("#email").val().trim())) {
      $("#email").removeClass("is-invalid");
      $("#email").addClass("is-valid");
    } else {
      $("#email").removeClass("is-valid");
      $("#email").addClass("is-invalid");
    }
  });

  //檢查密碼合乎密碼規則
  $("#password").on("input", function () {
    if (passwordRule.test($("#password").val().trim())) {
      $("#password").removeClass("is-invalid");
      $("#password").addClass("is-valid");
    } else {
      $("#password").removeClass("is-valid");
      $("#password").addClass("is-invalid");
    }
  });

  // 檢查員工編號不為空白
  $("#admin").on("input", function () {
    if ($("#admin").val().trim() != "") {
      $("#admin").removeClass("is-invalid");
      $("#admin").addClass("is-valid");
    } else {
      $("#admin").removeClass("is-valid");
      $("#admin").addClass("is-invalid");
    }
  });

  // 檢查表單內容後送出資料
  $("#submit").click(function () {
    // 若有不合規範的輸入值則顯示錯誤訊息並不送出更新
    if (
      $(".is-invalid").html() != undefined ||
      $("#lastName").val().trim() == "" ||
      $("#firstName").val().trim() == "" ||
      $("#email").val().trim() == "" ||
      $("#password").val().trim() == "" ||
      $("#admin").val().trim() == ""
    ) {
      alert("請修正表單內容！");

      return false;
    }

    // 建立要送出資料的json
    let data = {
      firstName: $("#firstName").val().trim(),
      lastName: $("#lastName").val().trim(),
      email: $("#email").val().trim(),
      password: $("#password").val().trim(),
      admin: $("#admin").val().trim(),
    };
    console.log(data);
    // 轉成string以供後端讀取
    const dataString = JSON.stringify(data);

    $.ajax({
      url: "api",
      type: "POST",
      data: dataString,
      crossDomain: true,
      cache: false,
      dataType: "json",
      timeout: 5000,
      success: function (data) {
        console.log(data);

        // 根據response訊息進行不同動作
        if ("errors" in data) {
          alert(data.errors);
        } else if ("message" in data) {
          alert(data.message);

          window.location = "/co_kitchen/account/list";
        }
      },
      error: function (error) {
        console.log(error);

        // 顯示錯誤訊息
        alert(
          "Error code: " +
            error.status +
            "\n\nDescribtion: \n" +
            error.statusText
        );
      },
    });
  });
});
