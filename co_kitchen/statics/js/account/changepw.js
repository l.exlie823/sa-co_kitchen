/* function loadContent(id, admin) {
  if (id != this.urlId) {
    window.location = "/co_kitchen";
  }
} */

$(document).ready(function () {
  // 啟用Bootstrap tooltip功能
  var tooltipTriggerList = [].slice.call(
    document.querySelectorAll('[data-bs-toggle="tooltip"]')
  );
  var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl);
  });

  let lastName, firstName, email;

  // 取得url上的參數
  const urlParams = new URLSearchParams(window.location.search);
  const urlId = urlParams.get("id");

  // 確認有要求id才進行資料查詢，否則重導至首頁
  if (urlId == null || urlId == "") {
    window.location = "/co_kitchen";
  } else {
    $.ajax({
      url: "api",
      type: "GET",
      crossDomain: true,
      cache: false,
      data: "id=" + urlId,
      dataType: "json",
      timeout: 5000,
      success: function (data) {
        console.log(data);

        // 根據response訊息進行不同動作
        if ("errors" in data) {
          alert(data.errors);
        } else if ("message" in data) {
          // 紀錄帳號資料以回傳進行更新
          lastName = data.result.data[0]["lastName"];
          firstName = data.result.data[0]["firstName"];
          email = data.result.data[0]["email"];
        }
      },
      error: function (error) {
        console.log(error);

        // 顯示錯誤訊息
        alert(
          "Error code: " +
            error.status +
            "\n\nDescribtion: \n" +
            error.statusText
        );
      },
    });
  }

  // 信箱與密碼格式規則
  const password_rule = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;

  //檢查密碼合乎密碼規則
  $("#password").on("input", function () {
    if (password_rule.test($("#password").val().trim())) {
      $("#password").removeClass("is-invalid");
      $("#password").addClass("is-valid");
    } else {
      $("#password").removeClass("is-valid");
      $("#password").addClass("is-invalid");
    }
  });

  // 檢查確認密碼與密碼相同
  $("#confirmPassword").on("input", function () {
    if ($("#password").val() == $("#confirmPassword").val()) {
      $("#confirmPassword").removeClass("is-invalid");
      $("#confirmPassword").addClass("is-valid");
    } else {
      $("#confirmPassword").removeClass("is-valid");
      $("#confirmPassword").addClass("is-invalid");
    }
  });

  // 檢查表單內容後送出資料
  $("#submit").click(function () {
    // 若有不合規範的輸入值則顯示錯誤訊息並不送出更新
    if (
      $(".is-invalid").html() != undefined ||
      $("#password").val().trim() == "" ||
      $("#confirmPassword").val().trim() == ""
    ) {
      alert("請修正表單內容！");

      return false;
    }

    // 建立要送出資料的json
    let data = {
      id: parseInt(id, 10),
      firstName: firstName,
      lastName: lastName,
      email: email,
      password: $("#password").val().trim(),
      admin: 0,
    };
    console.log(data);
    // 轉成string以供後端讀取
    const dataString = JSON.stringify(data);

    $.ajax({
      url: "api",
      type: "PUT",
      data: dataString,
      crossDomain: true,
      cache: false,
      dataType: "json",
      timeout: 5000,
      success: function (data) {
        console.log(data);

        // 根據response訊息進行不同動作
        if ("errors" in data) {
          alert(data.errors);
        } else if ("message" in data) {
          alert(data.message);

          // 重導至帳號資料頁面
          window.location = "/co_kitchen/account/profile.html?id=" + id;
        }
      },
      error: function (error) {
        console.log(error);

        // 顯示錯誤訊息
        alert(
          "Error code: " +
            error.status +
            "\n\nDescribtion: \n" +
            error.statusText
        );
      },
    });
  });
});
