$(document).ready(function () {
  // 取得url上的參數
  const urlParams = new URLSearchParams(window.location.search);
  const id = urlParams.get("id");

  let password;

  // 確認有要求id才進行資料查詢，否則重導至首頁
  if (id == null || id == "") {
    window.location = "/co_kitchen";
  } else {
    $.ajax({
      url: "api",
      type: "GET",
      crossDomain: true,
      cache: false,
      data: "id=" + id,
      dataType: "json",
      timeout: 5000,
      success: function (data) {
        console.log(data);

        // 根據response訊息進行不同動作
        if ("errors" in data) {
          alert(data.errors);
        } else if ("message" in data) {
          // 填入資料與紀錄帳號資料以回傳進行更新
          $("#lastName").val(data.result.data[0]["lastName"]);
          $("#firstName").val(data.result.data[0]["firstName"]);
          $("#email").val(data.result.data[0]["email"]);
          $("#admin").val(data.result.data[0]["admin"]);
          password = data.result.data[0]["password"];
        }
      },
      error: function (error) {
        console.log(error);

        // 顯示錯誤訊息
        alert(
          "Error code: " +
            error.status +
            "\n\nDescribtion: \n" +
            error.statusText
        );
      },
    });
  }

  // 信箱與密碼格式規則
  const email_rule = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/;

  // 檢查姓氏不為空白
  $("#lastName").on("input", function () {
    if ($("#lastName").val().trim() != "") {
      $("#lastName").removeClass("is-invalid");
      $("#lastName").addClass("is-valid");
    } else {
      $("#lastName").removeClass("is-valid");
      $("#lastName").addClass("is-invalid");
    }
  });

  // 檢查名字不為空白
  $("#firstName").on("input", function () {
    if ($("#firstName").val().trim() != "") {
      $("#firstName").removeClass("is-invalid");
      $("#firstName").addClass("is-valid");
    } else {
      $("#firstName").removeClass("is-valid");
      $("#firstName").addClass("is-invalid");
    }
  });

  //檢查信箱合乎信箱規則
  $("#email").on("input", function () {
    if (email_rule.test($("#email").val().trim())) {
      $("#email").removeClass("is-invalid");
      $("#email").addClass("is-valid");
    } else {
      $("#email").removeClass("is-valid");
      $("#email").addClass("is-invalid");
    }
  });

  //檢查員工編號不為空白
  $("#admin").on("input", function () {
    if ($("#admin").val().trim()) {
      $("#admin").removeClass("is-invalid");
      $("#admin").addClass("is-valid");
    } else {
      $("#admin").removeClass("is-valid");
      $("#admin").addClass("is-invalid");
    }
  });

  // 檢查表單內容後送出資料
  $("#submit").click(function () {
    // 若有不合規範的輸入值則顯示錯誤訊息並不送出更新
    if (
      $(".is-invalid").html() != undefined ||
      $("#lastName").val().trim() == "" ||
      $("#firstName").val().trim() == "" ||
      $("#email").val().trim() == "" ||
      $("#admin").val().trim() == ""
    ) {
      alert("請修正表單內容！");

      return false;
    }

    // 建立要送出資料的json
    let data = {
      id: parseInt(id, 10),
      firstName: $("#firstName").val().trim(),
      lastName: $("#lastName").val().trim(),
      email: $("#email").val().trim(),
      password: password,
      admin: $("#admin").val().trim(),
    };
    console.log(data);
    // 轉成string以供後端讀取
    const dataString = JSON.stringify(data);

    $.ajax({
      url: "api",
      type: "PUT",
      data: dataString,
      crossDomain: true,
      cache: false,
      dataType: "json",
      timeout: 5000,
      success: function (data) {
        console.log(data);

        // 根據response訊息進行不同動作
        if ("errors" in data) {
          alert(data.errors);
        } else if ("message" in data) {
          alert(data.message);

          // 重導至帳號資料頁面
          window.location = "/co_kitchen/account/profile.html?id=" + id;
        }
      },
      error: function (error) {
        console.log(error);

        // 顯示錯誤訊息
        alert(
          "Error code: " +
            error.status +
            "\n\nDescribtion: \n" +
            error.statusText
        );
      },
    });
  });
});
