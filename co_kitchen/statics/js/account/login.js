$(document).ready(function () {
  // 信箱格式規則
  const emailRule = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/;

  //檢查信箱合乎信箱規則
  $("#email").on("input", function () {
    if (emailRule.test($("#email").val().trim())) {
      $("#email").removeClass("is-invalid");
      $("#email").addClass("is-valid");
    } else {
      $("#email").removeClass("is-valid");
      $("#email").addClass("is-invalid");
    }
  });

  // 檢查表單內容後送出資料
  $("#submit").click(function () {
    // 若有不合規範的輸入值則顯示錯誤訊息並不送出更新
    if (
      $(".is-invalid").html() != undefined ||
      $("#email").val().trim() == "" ||
      $("#password").val().trim() == ""
    ) {
      alert("請修正表單內容！");

      return false;
    }

    // 建立要送出資料的json
    let data = {
      email: $("#email").val().trim(),
      password: $("#password").val().trim(),
    };
    console.log(data);
    // 轉成string以供後端讀取
    const dataString = JSON.stringify(data);

    $.ajax({
      url: "/co_kitchen/auth/api",
      type: "POST",
      data: dataString,
      crossDomain: true,
      cache: false,
      dataType: "json",
      // timeout: 5000,
      success: function (data) {
        console.log(data);

        // 根據response訊息進行不同動作
        if ("errors" in data) {
          alert(data.errors);
        } else if ("message" in data) {
          alert(data.message);

          // 重導至首頁
          window.location = "/co_kitchen";
        }
      },
      error: function (error) {
        console.log(error);

        // 顯示錯誤訊息
        alert(
          "Error code: " +
            error.status +
            "\n\nDescribtion: \n" +
            error.statusText
        );
      },
    });
  });
});
