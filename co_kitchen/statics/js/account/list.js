$(document).ready(function () {
  let accountList = null;
  // 取得所有帳號資料
  $.ajax({
    url: "api",
    type: "GET",
    crossDomain: true,
    cache: false,
    dataType: "json",
    timeout: 5000,
    success: function (data) {
      console.log(data);

      // 根據response訊息進行不同動作
      if ("errors" in data) {
        alert(data.errors);
      } else if ("message" in data) {
        accountList = data.result.data;

        // 將每一筆資料依序放入表格
        accountList.forEach((element) => {
          updateTable(element);
        });
      }
    },
    error: function (error) {
      console.log(error);

      // 顯示錯誤訊息
      alert(
        "Error code: " + error.status + "\n\nDescribtion: \n" + error.statusText
      );
    },
  });

  /**
   * 填入取得的帳號資料
   *
   * @param element 欲填入資料
   */
  function updateTable(element) {
    // 複製表格內容模板
    clone = $("#tbody-template").clone(true, true);
    // 移除隱藏用的CSS class
    clone.removeClass("d-none");

    // 設定該區塊的id
    clone.attr("id", element["id"]);
    // 填入帳號資料
    clone.find("#id").text(element["id"]);
    clone.find("#name").text(element["lastName"] + " " + element["firstName"]);
    clone.find("#email").text(element["email"]);
    clone.find("#admin").text(element["admin"]);
    clone.find("#deleted").text(element["deleted"] == true ? "T" : "F");
    // 設定button的href
    clone.find("#profile").attr("href", "profile.html?id=" + element["id"]);
    clone.find("#edit").attr("href", "edit.html?id=" + element["id"]);

    // 針對已軟刪除的行做調整
    if (element["deleted"]) {
      // 調整為不明顯的底色
      clone.addClass("bg-light");

      // 使編輯與刪除鈕失效
      clone.find("#edit").addClass("disabled");
      clone.find("#delete").addClass("disabled");
    }

    // 放入最後一行
    clone.appendTo("tbody");
  }

  $("#search #submit").click(function () {
    $("tbody").children().slice(1).remove();

    let deleted;

    if ($("#search #deleted option:selected").val() == "T") {
      deleted = true;
    } else if ($("#search #deleted option:selected").val() == "F") {
      deleted = false;
    }

    accountList.forEach((element) => {
      if (
        ($("#search #id").val() == "" ||
          element["id"] == parseInt($("#search #id").val(), 10)) &&
        element["lastName"].includes($("#search #lastName").val()) &&
        element["firstName"].includes($("#search #firstName").val()) &&
        element["email"].includes($("#search #email").val()) &&
        ($("#search #admin").val() == "" ||
          element["admin"] == $("#search #admin").val()) &&
        ($("#search #deleted option:selected").val() == "" ||
          element["deleted"] == deleted)
      ) {
        updateTable(element);
      }
    });
  });

  // 確認刪除意願後進行刪除
  $("#delete").click(function () {
    // 取得要刪除的id
    const id = $(this).parents("tr").attr("id");

    // 建立要送出資料的json
    let data = {
      id: id,
    };
    // 轉成string以供後端讀取
    const dataString = JSON.stringify(data);

    // 確認刪除意願，同意後進行刪除
    if (confirm("確定刪除編號" + id + "的帳號？")) {
      $.ajax({
        url: "api",
        type: "DELETE",
        crossDomain: true,
        cache: false,
        data: dataString,
        dataType: "json",
        timeout: 5000,
        success: function (data) {
          console.log(data);

          // 根據response訊息進行不同動作
          if ("errors" in data) {
            alert(data.errors);
          } else if ("message" in data) {
            alert(data.message);

            // 刷新頁面
            location.reload();
          }
        },
        error: function (error) {
          console.log(error);

          // 顯示錯誤訊息
          alert(
            "Error code: " +
              error.status +
              "\n\nDescribtion: \n" +
              error.statusText
          );
        },
      });
    }
  });
});
