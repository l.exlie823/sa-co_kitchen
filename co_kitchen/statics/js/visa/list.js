$(document).ready(function () {
  // 取得所有信用卡資料
  $.ajax({
    url: "api",
    type: "GET",
    crossDomain: true,
    cache: false,
    dataType: "json",
    timeout: 5000,
    success: function (data) {
      console.log(data);

      // 根據response訊息進行不同動作
      if ("errors" in data) {
        alert(data.errors);
      } else if ("message" in data) {
        // 將每一筆資料依序放入表格
        data.result.data.forEach((element) => {
          updateTable(element);
        });
      }
    },
    error: function (error) {
      console.log(error);

      // 顯示錯誤訊息
      alert(
        "Error code: " + error.status + "\n\nDescribtion: \n" + error.statusText
      );
    },
  });

  /**
   * 填入取得的信用卡資料
   *
   * @param element 欲填入資料
   */
  function updateTable(element) {
    // 複製表格內容模板
    clone = $("#tbody-template").clone(true, true);
    // 移除隱藏用的CSS class
    clone.removeClass("d-none");

    // 設定該區塊的id
    clone.attr("id", element["id"]);
    // 填入信用卡資料
    clone.find("#id").text(element["id"]);
    clone.find("#accountId").text(element["accountId"]);
    clone.find("#visaId").text(element["visaId"]);
    clone.find("#bankId").text(element["bankId"]);
    clone.find("#cardId").text(element["cardId"].match(/\d{4}/g).join("-"));
    clone.find("#validationNum").text(element["validationNum"]);
    let goodThru = new Date(element["goodThru"]);
    clone
      .find("#goodThru")
      .text(goodThru.getMonth() + 1 + " / " + goodThru.getFullYear());
    clone.find("#deleted").text(element["deleted"] == true ? "T" : "F");
    // 設定button的href
    clone
      .find("#information")
      .attr("href", "information.html?id=" + element["id"]);
    clone.find("#edit").attr("href", "edit.html?id=" + element["id"]);

    // 針對已軟刪除的行做調整
    if (element["deleted"]) {
      // 調整為不明顯的底色
      clone.addClass("bg-light");

      // 使編輯與刪除鈕失效
      clone.find("#edit").addClass("disabled");
      clone.find("#delete").addClass("disabled");
    }

    // 放入最後一行
    clone.appendTo("tbody");
  }

  // 確認刪除意願後進行刪除
  $("#delete").click(function () {
    // 取得要刪除的id
    const id = $(this).parents("tr").attr("id");

    // 建立要送出資料的json
    let data = {
      id: id,
    };
    // 轉成string以供後端讀取
    const dataString = JSON.stringify(data);

    // 確認刪除意願，同意後進行刪除
    if (confirm("確定刪除編號" + id + "的信用卡？")) {
      $.ajax({
        url: "api",
        type: "DELETE",
        crossDomain: true,
        cache: false,
        data: dataString,
        dataType: "json",
        timeout: 5000,
        success: function (data) {
          console.log(data);

          // 根據response訊息進行不同動作
          if ("errors" in data) {
            alert(data.errors);
          } else if ("message" in data) {
            alert(data.message);

            // 刷新頁面
            location.reload();
          }
        },
        error: function (error) {
          console.log(error);

          // 顯示錯誤訊息
          alert(
            "Error code: " +
              error.status +
              "\n\nDescribtion: \n" +
              error.statusText
          );
        },
      });
    }
  });
});
