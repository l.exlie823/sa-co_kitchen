$(document).ready(function () {
  // 取得url上的參數
  const urlParams = new URLSearchParams(window.location.search);
  const id = urlParams.get("id");

  // 確認有要求id才進行資料查詢，否則重導至首頁
  if (id == null || id == "") {
    window.location = "/co_kitchen";
  } else {
    // 填入button href
    $("#edit").attr("href", "edit.html?id=" + id);
    $("#changepw").attr("href", "changepw.html?id=" + id);

    $.ajax({
      url: "api",
      type: "GET",
      crossDomain: true,
      cache: false,
      data: "id=" + id,
      dataType: "json",
      timeout: 5000,
      success: function (data) {
        console.log(data);

        if ("errors" in data) {
          alert(data.errors);
        } else if ("message" in data) {
          $("#id").text(data.result.data[0]["id"]);
          $("#accountId").text(data.result.data[0]["accountId"]);
          $("#bankId").text(data.result.data[0]["bankId"]);
          $("#cardId").text(
            data.result.data[0]["cardId"].match(/\d{4}/g).join("-")
          );
          $("#validationNum").text(data.result.data[0]["validationNum"]);
          let goodThru = new Date(data.result.data[0]["goodThru"]);
          $("#goodThru").text(
            goodThru.getMonth() + 1 + " / " + goodThru.getFullYear()
          );
          $("#deleted").text(data.result.data[0]["deleted"] ? "T" : "F");
        }
      },
      error: function (error) {
        console.log(error);
        alert(
          "Error code: " +
            error.status +
            "\n\nDescribtion: \n" +
            error.statusText
        );
      },
    }).then(function () {
      if ($("#deleted").text() == "T") {
        // 使編輯與刪除鈕失效
        $("#edit").addClass("disabled");
        $("#delete").addClass("disabled");
      }
    });
  }

  // 確認刪除意願後進行信用卡刪除
  $("#delete").click(function () {
    // 建立要送出資料的json
    let data = {
      id: id,
    };
    // 轉成string以供後端讀取
    const dataString = JSON.stringify(data);

    // 確認刪除意願，同意後進行刪除
    if (confirm("確定刪除此信用卡？")) {
      $.ajax({
        url: "api",
        type: "DELETE",
        crossDomain: true,
        cache: false,
        data: dataString,
        dataType: "json",
        timeout: 5000,
        success: function (data) {
          console.log(data);

          // 根據response訊息進行不同動作
          if ("errors" in data) {
            alert(data.errors);
          } else if ("message" in data) {
            alert(data.message);

            // 刷新頁面
            location.reload();
          }
        },
        error: function (error) {
          console.log(error);

          // 顯示錯誤訊息
          alert(
            "Error code: " +
              error.status +
              "\n\nDescribtion: \n" +
              error.statusText
          );
        },
      });
    }
  });
});
