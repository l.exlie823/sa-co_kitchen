$(document).ready(function () {
  // 取得url上的參數
  const urlParams = new URLSearchParams(window.location.search);
  const id = urlParams.get("id");

  let accountId;

  // 確認有要求id才進行資料查詢，否則重導至首頁
  if (id == null || id == "") {
    window.location = "information.html";
  } else {
    $.ajax({
      url: "api",
      type: "GET",
      crossDomain: true,
      cache: false,
      data: "id=" + id,
      dataType: "json",
      timeout: 5000,
      success: function (data) {
        console.log(data);

        // 根據response訊息進行不同動作
        if ("errors" in data) {
          alert(data.errors);
        } else if ("message" in data) {
          // 填入資料與紀錄帳號資料以回傳進行更新
          accountId = data.result.data[0]["accountId"];
          $("#bankId").val(data.result.data[0]["bankId"]);
          $("#cardId1").val(data.result.data[0]["cardId"].slice(0, 4));
          $("#cardId2").val(data.result.data[0]["cardId"].slice(4, 8));
          $("#cardId3").val(data.result.data[0]["cardId"].slice(8, 12));
          $("#cardId4").val(data.result.data[0]["cardId"].slice(12));
          $("#validationNum").val(data.result.data[0]["validationNum"]);
          let goodThru = new Date(data.result.data[0]["goodThru"]);
          $("#goodThru1").val(goodThru.getMonth() + 1);
          $("#goodThru2").val(goodThru.getFullYear());
        }
      },
      error: function (error) {
        console.log(error);

        // 顯示錯誤訊息
        alert(
          "Error code: " +
            error.status +
            "\n\nDescribtion: \n" +
            error.statusText
        );
      },
    });
  }
  // 銀行行號規則
  const bankIdRule = /\d{3}/;
  // 卡號規則
  const cardIdRule = /\d{4}/;
  // 驗證碼規則
  const validationNumRule = /\d{3}/;

  // 檢查銀行行號不為空白
  $("#bankId").on("input", function () {
    if (
      $("#bankId").val().trim() != "" &&
      bankIdRule.test($("#bankId").val())
    ) {
      $("#bankId").removeClass("is-invalid");
      $("#bankId").addClass("is-valid");
    } else {
      $("#bankId").removeClass("is-valid");
      $("#bankId").addClass("is-invalid");
    }
  });

  // 檢查卡號1不為空白且合乎規則
  $("#cardId1").on("input", function () {
    if (
      $("#cardId1").val().trim() != "" &&
      cardIdRule.test($("#cardId1").val())
    ) {
      $("#cardId1").removeClass("is-invalid");
      $("#cardId1").addClass("is-valid");
    } else {
      $("#cardId1").removeClass("is-valid");
      $("#cardId1").addClass("is-invalid");
    }
  });

  // 檢查卡號2不為空白且合乎規則
  $("#cardId2").on("input", function () {
    if (
      $("#cardId2").val().trim() != "" &&
      cardIdRule.test($("#cardId2").val())
    ) {
      $("#cardId2").removeClass("is-invalid");
      $("#cardId2").addClass("is-valid");
    } else {
      $("#cardId2").removeClass("is-valid");
      $("#cardId2").addClass("is-invalid");
    }
  });

  // 檢查卡號3不為空白且合乎規則
  $("#cardId3").on("input", function () {
    if (
      $("#cardId3").val().trim() != "" &&
      cardIdRule.test($("#cardId3").val())
    ) {
      $("#cardId3").removeClass("is-invalid");
      $("#cardId3").addClass("is-valid");
    } else {
      $("#cardId3").removeClass("is-valid");
      $("#cardId3").addClass("is-invalid");
    }
  });

  // 檢查卡號4不為空白且合乎規則
  $("#cardId4").on("input", function () {
    if (
      $("#cardId4").val().trim() != "" &&
      cardIdRule.test($("#cardId4").val())
    ) {
      $("#cardId4").removeClass("is-invalid");
      $("#cardId4").addClass("is-valid");
    } else {
      $("#cardId4").removeClass("is-valid");
      $("#cardId4").addClass("is-invalid");
    }
  });

  // 檢查卡號驗證狀況
  $("#cardId input").on("input", function () {
    if ($("#cardId input").hasClass("is-invalid")) {
      $("#cardId").removeClass("is-valid");
      $("#cardId").addClass("is-invalid");
    } else {
      $("#cardId").removeClass("is-invalid");
      $("#cardId").addClass("is-valid");
    }
  });

  // 檢查驗證碼不為空白且合乎規則
  $("#validationNum").on("input", function () {
    if (
      $("#validationNum").val().trim() != "" &&
      validationNumRule.test($("#validationNum").val())
    ) {
      $("#validationNum").removeClass("is-invalid");
      $("#validationNum").addClass("is-valid");
    } else {
      $("#validationNum").removeClass("is-valid");
      $("#validationNum").addClass("is-invalid");
    }
  });

  // 檢查有效月不為空白
  $("#goodThru1").on("input", function () {
    if (
      $("#goodThru1").val().trim() != "" &&
      parseInt($("#goodThru1").val().trim(), 10) >= 1 &&
      parseInt($("#goodThru1").val().trim(), 10) <= 12
    ) {
      $("#goodThru1").removeClass("is-invalid");
      $("#goodThru1").addClass("is-valid");
    } else {
      $("#goodThru1").removeClass("is-valid");
      $("#goodThru1").addClass("is-invalid");
    }
  });

  // 檢查有效年不為空白
  $("#goodThru2").on("input", function () {
    if (
      $("#goodThru2").val().trim() != "" &&
      parseInt($("#goodThru2").val().trim(), 10) >= 2020
    ) {
      $("#goodThru2").removeClass("is-invalid");
      $("#goodThru2").addClass("is-valid");
    } else {
      $("#goodThru2").removeClass("is-valid");
      $("#goodThru2").addClass("is-invalid");
    }
  });

  // 檢查有效年月驗證狀況
  $("#goodThru input").on("input", function () {
    if ($("#goodThru input").hasClass("is-invalid")) {
      $("#goodThru").removeClass("is-valid");
      $("#goodThru").addClass("is-invalid");
    } else {
      $("#goodThru").removeClass("is-invalid");
      $("#goodThru").addClass("is-valid");
    }
  });

  // 檢查表單內容後送出資料
  $("#submit").click(function () {
    // 若有不合規範的輸入值則顯示錯誤訊息並不送出更新
    if (
      $(".is-invalid").html() != undefined ||
      $("#bankId").val().trim() == "" ||
      $("#cardId1").val().trim() == "" ||
      $("#cardId2").val().trim() == "" ||
      $("#cardId3").val().trim() == "" ||
      $("#cardId4").val().trim() == "" ||
      $("#validationNum").val().trim() == "" ||
      $("#goodThru1").val().trim() == "" ||
      $("#goodThru2").val().trim() == ""
    ) {
      alert("請修正表單內容！");

      return false;
    }

    const cardId =
      $("#cardId1").val() +
      $("#cardId2").val() +
      $("#cardId3").val() +
      $("#cardId4").val();

    const goodThru = new Date($("#goodThru2").val(), $("#goodThru1").val());
    goodThru.setTime(goodThru.getTime() - 1);

    // 建立要送出資料的json
    let data = {
      id: parseInt(id, 10),
      accountId: parseInt(accountId, 10),
      bankId: $("#bankId").val().trim(),
      cardId: cardId,
      validationNum: $("#validationNum").val().trim(),
      goodThru: goodThru.getTime(),
    };
    console.log(data);
    // 轉成string以供後端讀取
    const dataString = JSON.stringify(data);

    $.ajax({
      url: "api",
      type: "PUT",
      data: dataString,
      crossDomain: true,
      cache: false,
      dataType: "json",
      timeout: 5000,
      success: function (data) {
        console.log(data);

        // 根據response訊息進行不同動作
        if ("errors" in data) {
          alert(data.errors);
        } else if ("message" in data) {
          alert(data.message);

          // 重導至首頁
          window.location = "/co_kitchen";
        }
      },
      error: function (error) {
        console.log(error);

        // 顯示錯誤訊息
        alert(
          "Error code: " +
            error.status +
            "\n\nDescribtion: \n" +
            error.statusText
        );
      },
    });
  });
});
