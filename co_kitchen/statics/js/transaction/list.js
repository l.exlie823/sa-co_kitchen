$(document).ready(function () {
  // 取得所有交易資料
  $.ajax({
    url: "api",
    type: "GET",
    crossDomain: true,
    cache: false,
    dataType: "json",
    timeout: 5000,
    success: function (data) {
      console.log(data);

      // 根據response訊息進行不同動作
      if ("errors" in data) {
        alert(data.errors);
      } else if ("message" in data) {
        // 將每一筆資料依序放入表格
        data.result.data.forEach((element) => {
          updateTable(element);
        });
      }
    },
    error: function (error) {
      console.log(error);

      // 顯示錯誤訊息
      alert(
        "Error code: " + error.status + "\n\nDescribtion: \n" + error.statusText
      );
    },
  });

  /**
   * 填入取得的交易資料
   *
   * @param element 欲填入資料
   */
  function updateTable(element) {
    // 複製表格內容模板
    clone = $("#tbody-template").clone(true, true);
    // 移除隱藏用的CSS class
    clone.removeClass("d-none");

    // 設定該區塊的id
    clone.attr("id", element["id"]);
    // 填入交易資料
    clone.find("#id").text(element["id"]);
    clone.find("#accountId").text(element["accountId"]);
    clone.find("#rentId").text(element["rentId"]);
    clone.find("#visaId").text(element["visaId"]);

    // 放入最後一行
    clone.appendTo("tbody");
  }
});
