$(document).ready(function () {
  let id, admin;

  /** 載入header.html並調整顯示內容 */
  $("#header").load("/co_kitchen/header.html", function () {
    /** 查詢登入狀態 */
    $.ajax({
      url: "/co_kitchen/auth/api",
      type: "GET",
      crossDomain: true,
      cache: false,
      dataType: "json",
      success: function (data) {
        console.log(data);

        // 根據response訊息進行不同動作
        if ("errors" in data) {
          alert(data.errors);
        } else if ("message" in data) {
          id = data.id;
          admin = data.admin;
        }

        /** 調整顯示內容 */
        if (id > 0) {
          $("#rent").removeClass("d-none");
          $("#rentList").attr(
            "href",
            "/co_kitchen/rent/list.html?accountId=" + id
          );
          $("#visa").removeClass("d-none");
          $("#visaList").attr(
            "href",
            "/co_kitchen/visa/list.html?accountId=" + id
          );
          $("#account").removeClass("d-none");
          $("#accountProfile").attr(
            "href",
            "/co_kitchen/account/profile.html?id=" + id
          );
          $("#transactionList").attr(
            "href",
            "/co_kitchen/transaction/list.html?accountId=" + id
          );
          $("#login").addClass("d-none");
          $("#register").addClass("d-none");

          if (admin) {
            $("#allRentList").removeClass("d-none");
            $("#kitchenList").addClass("d-none");
            $("#navKitchen").removeClass("d-none");
            $("#allVisaList").removeClass("d-none");
            $("#allTransactionList").removeClass("d-none");
            $("#addAdmin").removeClass("d-none");
            $("#accountList").removeClass("d-none");
          }
        }

        /** 載入其他需要登入驗證資訊的程式 */
        if (typeof loadContent === "function") {
          loadContent(admin);
        }
      },
      error: function (error) {
        console.log(error);

        // 顯示錯誤訊息
        alert(
          "Error code: " +
            error.status +
            "\n\nDescribtion: \n" +
            error.statusText
        );
      },
    });

    /** 點擊登出事件 */
    $("#logout").click(function () {
      $.ajax({
        url: "/co_kitchen/auth/api",
        type: "DELETE",
        crossDomain: true,
        cache: false,
        dataType: "json",
        success: function (data) {
          console.log(data);

          // 根據response訊息進行不同動作
          if ("errors" in data) {
            alert(data.errors);
          } else if ("message" in data) {
            alert(data.message);

            window.location = "/co_kitchen";
          }
        },
        error: function (error) {
          console.log(error);

          // 顯示錯誤訊息
          alert(
            "Error code: " +
              error.status +
              "\n\nDescribtion: \n" +
              error.statusText
          );
        },
      });
    });
  });
});
