$(document).ready(function () {
  // 取得url上的參數
  const urlParams = new URLSearchParams(window.location.search);
  const id = urlParams.get("id");

  // 確認有要求id才進行資料查詢，否則重導至首頁
  if (id == null || id == "") {
    window.location = "/co_kitchen";
  } else {
    // 填入button href
    $("#edit").attr("href", "edit.html?id=" + id);

    $.ajax({
      url: "api",
      type: "GET",
      crossDomain: true,
      cache: false,
      data: "id=" + id,
      dataType: "json",
      timeout: 5000,
      success: function (data) {
        console.log(data);

        if ("errors" in data) {
          alert(data.errors);
        } else if ("message" in data) {
          $("#id").text(data.result.data[0]["id"]);
          $("#accountId").text(data.result.data[0]["accountId"]);
          $("#kitchenId").text(data.result.data[0]["kitchenId"]);
          let startAt = new Date(data.result.data[0]["startAt"]);
          $("#startAt").text(
            startAt.getFullYear() +
              "/" +
              String(startAt.getMonth() + 1).padStart(2, "0") +
              "/" +
              String(startAt.getDate()).padStart(2, "0") +
              " " +
              String(startAt.getHours()).padStart(2, "0") +
              ":" +
              String(startAt.getMinutes()).padStart(2, "0")
          );
          let endAt = new Date(data.result.data[0]["endAt"]);
          $("#endAt").text(
            endAt.getFullYear() +
              "/" +
              String(endAt.getMonth() + 1).padStart(2, "0") +
              "/" +
              String(endAt.getDate()).padStart(2, "0") +
              " " +
              String(endAt.getHours()).padStart(2, "0") +
              ":" +
              String(endAt.getMinutes()).padStart(2, "0")
          );
          $("#paid").text(data.result.data[0]["paid"] ? "T" : "F");
        }
      },
      error: function (error) {
        console.log(error);
        alert(
          "Error code: " +
            error.status +
            "\n\nDescribtion: \n" +
            error.statusText
        );
      },
    }).then(function () {
      if ($("#paid").text() == "T") {
        // 使編輯與刪除鈕失效
        $("#pay").addClass("disabled");
        $("#edit").addClass("disabled");
        $("#delete").addClass("disabled");
      }
    });
  }

  $("#pay").click(function () {
    $("#form-template").nextAll().remove();

    $.ajax({
      url: "/co_kitchen/visa/api",
      type: "GET",
      crossDomain: true,
      cache: false,
      data: "accountId=" + $("#accountId").text(),
      dataType: "json",
      timeout: 5000,
      success: function (data) {
        console.log(data);

        // 根據response訊息進行不同動作
        if ("errors" in data) {
          alert(data.errors);
        } else if ("message" in data) {
          // 將每一筆資料依序放入表格
          data.result.data.forEach((element) => {
            if (!element["deleted"]) {
              updateModal(element);
            }
          });
        }
      },
      error: function (error) {
        console.log(error);

        // 顯示錯誤訊息
        alert(
          "Error code: " +
            error.status +
            "\n\nDescribtion: \n" +
            error.statusText
        );
      },
    });
  });

  /**
   * 填入取得的信用卡資料
   *
   * @param element 欲填入資料
   */
  function updateModal(element) {
    // 複製表單內容模板
    clone = $("#form-template").clone(true, true);
    // 移除隱藏用的CSS class
    clone.removeClass("d-none");

    // 設定該區塊的id
    clone.attr("id", element["id"]);
    // 填入信用卡資料
    clone.find("input").val(element["id"]);
    clone
      .find("label[for='visaId']")
      .text(element["cardId"].match(/\d{4}/g).join("-"));

    // 放入最後一行
    clone.appendTo("form");
  }

  $("#payConfirm").click(function () {
    let data = {
      accountId: parseInt($("#accountId").text(), 10),
      rentId: parseInt($("#id").text(), 10),
      visaId: parseInt($("#visaId:checked").val(), 10),
    };
    console.log(data);

    const dataString = JSON.stringify(data);

    $.ajax({
      url: "/co_kitchen/transaction/api",
      type: "POST",
      crossDomain: true,
      cache: false,
      data: dataString,
      dataType: "json",
      timeout: 5000,
      success: function (data) {
        console.log(data);

        // 根據response訊息進行不同動作
        if ("errors" in data) {
          alert(data.errors);
        } else if ("message" in data) {
          alert(data.message);

          // 刷新頁面
          location.reload();
        }
      },
      error: function (error) {
        console.log(error);

        // 顯示錯誤訊息
        alert(
          "Error code: " +
            error.status +
            "\n\nDescribtion: \n" +
            error.statusText
        );
      },
    });
  });

  // 確認刪除意願後進行租用刪除
  $("#delete").click(function () {
    // 建立要送出資料的json
    let data = {
      id: id,
    };
    // 轉成string以供後端讀取
    const dataString = JSON.stringify(data);

    // 確認刪除意願，同意後進行刪除
    if (confirm("確定刪除此租用？")) {
      $.ajax({
        url: "api",
        type: "DELETE",
        crossDomain: true,
        cache: false,
        data: dataString,
        dataType: "json",
        timeout: 5000,
        success: function (data) {
          console.log(data);

          // 根據response訊息進行不同動作
          if ("errors" in data) {
            alert(data.errors);
          } else if ("message" in data) {
            alert(data.message);

            // 刷新頁面
            location.reload();
          }
        },
        error: function (error) {
          console.log(error);

          // 顯示錯誤訊息
          alert(
            "Error code: " +
              error.status +
              "\n\nDescribtion: \n" +
              error.statusText
          );
        },
      });
    }
  });
});
