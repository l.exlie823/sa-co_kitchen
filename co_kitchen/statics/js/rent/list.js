$(document).ready(function () {
  // 取得url上的參數
  const urlParams = new URLSearchParams(window.location.search);
  const accountId = urlParams.get("accountId");

  let rentList;
  let withHistory = false;

  if (accountId == null) {
    // 取得所有租用資料
    $.ajax({
      url: "api",
      type: "GET",
      crossDomain: true,
      cache: false,
      dataType: "json",
      timeout: 5000,
      success: function (data) {
        console.log(data);

        // 根據response訊息進行不同動作
        if ("errors" in data) {
          alert(data.errors);
        } else if ("message" in data) {
          rentList = data.result.data;

          updateQuery();
        }
      },
      error: function (error) {
        console.log(error);

        // 顯示錯誤訊息
        alert(
          "Error code: " +
            error.status +
            "\n\nDescribtion: \n" +
            error.statusText
        );
      },
    });
  } else {
    // 取得帳號租用資料
    $.ajax({
      url: "api",
      type: "GET",
      crossDomain: true,
      cache: false,
      data: "accountId=" + accountId,
      dataType: "json",
      timeout: 5000,
      success: function (data) {
        console.log(data);

        // 根據response訊息進行不同動作
        if ("errors" in data) {
          alert(data.errors);
        } else if ("message" in data) {
          rentList = data.result.data;

          updateQuery();
        }
      },
      error: function (error) {
        console.log(error);

        // 顯示錯誤訊息
        alert(
          "Error code: " +
            error.status +
            "\n\nDescribtion: \n" +
            error.statusText
        );
      },
    });
  }

  /**
   * 更新查詢條件
   */
  function updateQuery() {
    $("tbody").children().slice(1).remove();

    let paid;

    if ($("#search #paid option:selected").val() == "T") {
      paid = true;
    } else if ($("#search #paid option:selected").val() == "F") {
      paid = false;
    }

    rentList.forEach((element) => {
      let startAt = new Date($("#search #startAt").val());
      let endAt = new Date($("#search #endAt").val());
      let rStartAt = new Date(element["startAt"]);
      let rEndAt = new Date(element["endAt"]);
      let now = new Date();

      if (
        ($("#search #id").val() == "" ||
          element["id"] == parseInt($("#search #id").val(), 10)) &&
        ($("#search #accountId").val() == "" ||
          element["accountId"] ==
            parseInt($("#search #accountId").val(), 10)) &&
        ($("#search #kitchenId").val() == "" ||
          element["kitchenId"] ==
            parseInt($("#search #kitchenId").val(), 10)) &&
        ($("#search #startAt").val() == "" || rStartAt > startAt) &&
        ($("#search #endAt").val() == "" || rEndAt < endAt) &&
        ($("#search #priceFloor").val() == "" ||
          element["price"] >= parseInt($("#search #priceFloor").val(), 10)) &&
        ($("#search #priceCelling").val() == "" ||
          element["price"] <= parseInt($("#search #priceCelling").val(), 10)) &&
        ($("#search #paid option:selected").val() == "" ||
          element["paid"] == paid)
      ) {
        if (withHistory) {
          updateTable(element);
        } else {
          if (rEndAt >= now) {
            updateTable(element);
          }
        }
      }
    });
  }

  /**
   * 填入取得的租用資料
   *
   * @param element 欲填入資料
   */
  function updateTable(element) {
    // 複製表格內容模板
    clone = $("#tbody-template").clone(true, true);
    // 移除隱藏用的CSS class
    clone.removeClass("d-none");

    // 設定該區塊的id
    clone.attr("id", element["id"]);
    // 填入租用資料
    clone.find("#id").text(element["id"]);
    clone.find("#accountId").text(element["accountId"]);
    clone.find("#kitchenId").text(element["kitchenId"]);
    let startAt = new Date(element["startAt"]);
    clone
      .find("#startAt")
      .text(
        startAt
          .toISOString()
          .slice(0, 16)
          .replaceAll("-", "/")
          .replace("T", " ")
      );
    let endAt = new Date(element["endAt"]);
    clone
      .find("#endAt")
      .text(
        endAt.toISOString().slice(0, 16).replaceAll("-", "/").replace("T", " ")
      );
    clone.find("#price").text(element["price"]);
    if (element["paid"]) {
      clone.find("#paid").text("T");
    } else {
      clone.find("#paid").text("F");
    }
    // 設定button的href
    clone
      .find("#information")
      .attr("href", "information.html?id=" + element["id"]);
    clone.find("#edit").attr("href", "edit.html?id=" + element["id"]);

    // 針對已付款的行做調整
    if (element["paid"]) {
      // 使編輯與刪除鈕失效
      clone.find("#edit").addClass("disabled");
      clone.find("#delete").addClass("disabled");
    }

    // 放入最後一行
    clone.appendTo("tbody");
  }

  $("#withHistory").change(function () {
    if (this.checked) {
      withHistory = true;
    } else {
      withHistory = false;
    }

    updateQuery();
  });

  $("#search #submit").click(function () {
    updateQuery();
  });

  // 確認刪除意願後進行刪除
  $("#delete").click(function () {
    // 取得要刪除的id
    const id = $(this).parents("tr").attr("id");

    // 建立要送出資料的json
    let data = {
      id: id,
    };
    // 轉成string以供後端讀取
    const dataString = JSON.stringify(data);

    // 確認刪除意願，同意後進行刪除
    if (confirm("確定刪除編號" + id + "的租用？")) {
      $.ajax({
        url: "api",
        type: "DELETE",
        crossDomain: true,
        cache: false,
        data: dataString,
        dataType: "json",
        timeout: 5000,
        success: function (data) {
          console.log(data);

          // 根據response訊息進行不同動作
          if ("errors" in data) {
            alert(data.errors);
          } else if ("message" in data) {
            alert(data.message);

            // 刷新頁面
            location.reload();
          }
        },
        error: function (error) {
          console.log(error);

          // 顯示錯誤訊息
          alert(
            "Error code: " +
              error.status +
              "\n\nDescribtion: \n" +
              error.statusText
          );
        },
      });
    }
  });
});
