$(document).ready(function () {
  let pricePerHour;

  $("#selectKitchen").click(function () {
    $("#tbody-template").nextAll().remove();

    $.ajax({
      url: "/co_kitchen/kitchen/api",
      type: "GET",
      crossDomain: true,
      cache: false,
      dataType: "json",
      timeout: 5000,
      success: function (data) {
        console.log(data);

        // 根據response訊息進行不同動作
        if ("errors" in data) {
          alert(data.errors);
        } else if ("message" in data) {
          data.result.data.forEach((element) => {
            if (!element["deleted"]) {
              updateTable(element);
            }
          });
        }
      },
      error: function (error) {
        console.log(error);

        // 顯示錯誤訊息
        alert(
          "Error code: " +
            error.status +
            "\n\nDescribtion: \n" +
            error.statusText
        );
      },
    });
  });

  /**
   * 填入取得的廚房資料
   *
   * @param element 欲填入資料
   */
  function updateTable(element) {
    // 複製表格內容模板
    clone = $("#tbody-template").clone(true, true);
    // 移除隱藏用的CSS class
    clone.removeClass("d-none");

    // 設定該區塊的id
    clone.attr("id", element["id"]);
    // 填入廚房資料
    clone.find("#id").text(element["id"]);
    clone.find("#address").text(element["address"]);
    clone.find("#price").text(element["price"]);

    // 放入最後一行
    clone.appendTo("tbody");
  }

  // 選擇廚房
  $("#select").click(function () {
    $("#kitchen").removeClass("d-none");
    $("#kitchenId").val($(this).parents("tr").attr("id"));
    $("#kitchen #address").text($(this).parents("tr").find("#address").text());
    $("#kitchen #pricePerHour").text(
      $(this).parents("tr").find("#price").text()
    );

    pricePerHour = parseInt($(this).parents("tr").find("#price").text(), 10);

    if (pricePerHour != null) {
      calculatePrice();
    }
  });

  // 檢查租用開始時間不為空白
  $("#startAt").on("input", function () {
    if ($("#startAt").val().trim() != "") {
      $("#startAt").removeClass("is-invalid").addClass("is-valid");
      $("#endAt").attr("min", $("#startAt").val().trim());
      calculatePrice();
    } else {
      $("#startAt").removeClass("is-valid").addClass("is-invalid");
    }
  });

  // 檢查租用結束時間不為空白
  $("#endAt").on("input", function () {
    if ($("#endAt").val().trim() != "") {
      $("#endAt").removeClass("is-invalid").addClass("is-valid");
      $("#startAt").attr("max", $("#endAt").val().trim());
      calculatePrice();
    } else {
      $("#endAt").removeClass("is-valid").addClass("is-invalid");
    }
  });

  function calculatePrice() {
    if ($("#startAt").val().trim() != "" && $("#endAt").val().trim() != "") {
      let startAt = new Date($("#startAt").val().trim());
      let endAt = new Date($("#endAt").val().trim());

      if (startAt > endAt) {
        $("#startAt").removeClass("is-valid").addClass("is-invalid");
        $("#endAt").removeClass("is-valid").addClass("is-invalid");

        return;
      } else {
        $("#startAt").removeClass("is-invalid").addClass("is-valid");
        $("#endAt").removeClass("is-invalid").addClass("is-valid");
      }

      if (pricePerHour != null) {
        let timeSection = (endAt.getTime() - startAt.getTime()) / 1800000;
        $("#price").val((pricePerHour * Math.floor(timeSection)) / 2);
      }
    }
  }

  // 檢查表單內容後送出資料
  $("#submit").click(function () {
    // 若有不合規範的輸入值則顯示錯誤訊息並不送出更新
    if (
      $(".is-invalid").html() != undefined ||
      $("#kitchenId").val().trim() == "" ||
      $("#startAt").val().trim() == "" ||
      $("#endAt").val().trim() == ""
    ) {
      alert("請修正表單內容！");

      return false;
    }

    let startAt = new Date($("#startAt").val().trim());
    let endAt = new Date($("#endAt").val().trim());

    // 建立要送出資料的json
    let data = {
      kitchenId: parseInt($("#kitchenId").val().trim()),
      startAt: startAt.getTime(),
      endAt: endAt.getTime(),
      price: parseInt($("#price").val()),
    };
    console.log(data);
    // 轉成string以供後端讀取
    const dataString = JSON.stringify(data);

    $.ajax({
      url: "api",
      type: "POST",
      data: dataString,
      crossDomain: true,
      cache: false,
      dataType: "json",
      timeout: 5000,
      success: function (data) {
        console.log(data);

        // 根據response訊息進行不同動作
        if ("errors" in data) {
          alert(data.errors);
        } else if ("message" in data) {
          alert(data.message);

          // 重導至首頁
          window.location = "/co_kitchen";
        }
      },
      error: function (error) {
        console.log(error);

        // 顯示錯誤訊息
        alert(
          "Error code: " +
            error.status +
            "\n\nDescribtion: \n" +
            error.statusText
        );
      },
    });
  });
});
