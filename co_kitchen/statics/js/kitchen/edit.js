$(document).ready(function () {
  // 取得url上的參數
  const urlParams = new URLSearchParams(window.location.search);
  const id = urlParams.get("id");

  // 確認有要求id才進行資料查詢，否則重導至首頁
  if (id == null || id == "") {
    window.location = "/co_kitchen";
  } else {
    $.ajax({
      url: "api",
      type: "GET",
      crossDomain: true,
      cache: false,
      data: "id=" + id,
      dataType: "json",
      timeout: 5000,
      success: function (data) {
        console.log(data);

        // 根據response訊息進行不同動作
        if ("errors" in data) {
          alert(data.errors);
        } else if ("message" in data) {
          // 填入資料與紀錄帳號資料以回傳進行更新
          $("#id").val(data.result.data[0]["id"]);
          $("#address").val(data.result.data[0]["address"]);
          $("#price").val(data.result.data[0]["price"]);
        }
      },
      error: function (error) {
        console.log(error);

        // 顯示錯誤訊息
        alert(
          "Error code: " +
            error.status +
            "\n\nDescribtion: \n" +
            error.statusText
        );
      },
    });
  }

  // 地址規則
  const addressRule = /([\u4E00-\u9FCC\u3400-\u4DB5\uFA0E\uFA0F\uFA11\uFA13\uFA14\uFA1F\uFA21\uFA23\uFA24\uFA27-\uFA29]|[\ud840-\ud868][\udc00-\udfff]|\ud869[\udc00-\uded6\udf00-\udfff]|[\ud86a-\ud86c][\udc00-\udfff]|\ud86d[\udc00-\udf34\udf40-\udfff]|\ud86e[\udc00-\udc1d])+(縣|市)([\u4E00-\u9FCC\u3400-\u4DB5\uFA0E\uFA0F\uFA11\uFA13\uFA14\uFA1F\uFA21\uFA23\uFA24\uFA27-\uFA29]|[\ud840-\ud868][\udc00-\udfff]|\ud869[\udc00-\uded6\udf00-\udfff]|[\ud86a-\ud86c][\udc00-\udfff]|\ud86d[\udc00-\udf34\udf40-\udfff]|\ud86e[\udc00-\udc1d])+(鄉|鎮|市|區)([\u4E00-\u9FCC\u3400-\u4DB5\uFA0E\uFA0F\uFA11\uFA13\uFA14\uFA1F\uFA21\uFA23\uFA24\uFA27-\uFA29]|[\ud840-\ud868][\udc00-\udfff]|\ud869[\udc00-\uded6\udf00-\udfff]|[\ud86a-\ud86c][\udc00-\udfff]|\ud86d[\udc00-\udf34\udf40-\udfff]|\ud86e[\udc00-\udc1d])+(大道|路|街)([\u4E00-\u9FCC\u3400-\u4DB5\uFA0E\uFA0F\uFA11\uFA13\uFA14\uFA1F\uFA21\uFA23\uFA24\uFA27-\uFA29]|[\ud840-\ud868][\udc00-\udfff]|\ud869[\udc00-\uded6\udf00-\udfff]|[\ud86a-\ud86c][\udc00-\udfff]|\ud86d[\udc00-\udf34\udf40-\udfff]|\ud86e[\udc00-\udc1d]|\w)*\d+號/;

  // 檢查地址合乎地址規則
  $("#address").on("input", function () {
    if (addressRule.test($("#address").val().trim())) {
      $("#address").removeClass("is-invalid");
      $("#address").addClass("is-valid");
    } else {
      $("#address").removeClass("is-valid");
      $("#address").addClass("is-invalid");
    }
  });

  // 檢查價錢不為空白
  $("#price").on("input", function () {
    if ($("#price").val().trim() != "") {
      $("#price").removeClass("is-invalid");
      $("#price").addClass("is-valid");
    } else {
      $("#price").removeClass("is-valid");
      $("#price").addClass("is-invalid");
    }
  });

  // 檢查表單內容後送出資料
  $("#submit").click(function () {
    // 若有不合規範的輸入值則顯示錯誤訊息並不送出更新
    if (
      $(".is-invalid").html() != undefined ||
      $("#address").val().trim() == "" ||
      $("#price").val().trim() == ""
    ) {
      alert("請修正表單內容！");

      return false;
    }

    // 建立要送出資料的json
    let data = {
      id: parseInt(id, 10),
      price: $("#price").val().trim(),
      address: $("#address").val().trim(),
    };
    console.log(data);
    // 轉成string以供後端讀取
    const dataString = JSON.stringify(data);

    $.ajax({
      url: "api",
      type: "PUT",
      data: dataString,
      crossDomain: true,
      cache: false,
      dataType: "json",
      timeout: 5000,
      success: function (data) {
        console.log(data);

        // 根據response訊息進行不同動作
        if ("errors" in data) {
          alert(data.errors);
        } else if ("message" in data) {
          alert(data.message);

          // 重導至廚房清單頁面
          window.location = "/co_kitchen/kitchen/list.html";
        }
      },
      error: function (error) {
        console.log(error);

        // 顯示錯誤訊息
        alert(
          "Error code: " +
            error.status +
            "\n\nDescribtion: \n" +
            error.statusText
        );
      },
    });
  });
});
