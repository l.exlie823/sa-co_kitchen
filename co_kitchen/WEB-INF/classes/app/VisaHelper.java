package app;

import java.sql.*;
import java.text.SimpleDateFormat;

import org.json.*;

import util.DBMgr;

public class VisaHelper {
  /** 靜態變數，儲存VisaHelper物件 */
  private static VisaHelper vh;
  /** 儲存JDBC資料庫連線 */
  private Connection conn = null;
  /** 儲存JDBC預準備之SQL指令 */
  private PreparedStatement prestat = null;

  /**
   * Constructor<br>
   * 採用Singleton，確保物件唯一
   */
  private VisaHelper() {
  }

  /**
   * 取得VisaHelper物件
   *
   * @return VisaHelper VisaHelper物件
   */
  public static VisaHelper getHelper() {
    /** 檢查是否存在VisaHelper物件，不存在則建立一個 */
    if (vh == null) {
      vh = new VisaHelper();
    }

    /** 回傳VisaHelper物件 */
    return vh;
  }

  /**
   * 取得所有信用卡資料
   *
   * @return JSONObject 所有信用卡資料
   */
  public JSONObject getAll() {
    /** 新建Visa物件之visa變數，用以儲存查詢後的信用卡資料 */
    Visa visa = null;
    /** 儲存所有查詢到的信用卡資料，以JSONArray的方式儲存 */
    JSONArray jsa = new JSONArray();
    /** 儲存所有JDBC檢索後的結果，以pointer方式讀取 */
    ResultSet rs = null;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      /** SQL指令 */
      String sql = "SELECT * FROM `visas`";

      /** 預編譯SQL指令 */
      prestat = conn.prepareStatement(sql);
      /** 執行查詢之SQL指令並記錄回傳資料 */
      rs = prestat.executeQuery();

      /** 以while迴圈取得下一筆資料 */
      while (rs.next()) {
        /** 取出ResultSet的資料 */
        int id = rs.getInt("id");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date createdAt = rs.getTimestamp("created_at");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date updatedAt = rs.getTimestamp("updated_at");
        int accountId = rs.getInt("account_id");
        String bankId = rs.getString("bank_id");
        String cardId = rs.getString("card_id");
        String validationNum = rs.getString("validation_num");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date goodThru = rs.getDate("good_thru");
        boolean deleted = rs.getBoolean("deleted");

        /** 將每一筆資料建立一個新的Visa物件 */
        visa = new Visa(id, createdAt, updatedAt, accountId, bankId, cardId, validationNum, goodThru, deleted);
        /** 取出信用卡資料並封裝至JSONArray */
        jsa.put(visa.getData());
      }
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(rs, prestat, conn);
    }

    /** 將所有信用卡資料的JSONArray封裝成JSONObject回傳 */
    JSONObject response = new JSONObject();
    response.put("data", jsa);

    return response;
  }

  /**
   * 透過信用卡編號取得信用卡資料
   *
   * @param id 信用卡編號
   * @return JSONObject 信用卡資料
   */
  public JSONObject getById(String id) {
    /** 新建Visa物件之visa變數，用以儲存查詢後的信用卡資料 */
    Visa visa = null;
    /** 儲存所有查詢到的信用卡資料，以JSONArray的方式儲存 */
    JSONArray jsa = new JSONArray();
    /** 儲存所有JDBC檢索後的結果，以pointer方式讀取 */
    ResultSet rs = null;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      /** SQL指令 */
      String sql = "SELECT * FROM `visas` WHERE `id` = ?";

      /** 預編譯SQL指令 */
      prestat = conn.prepareStatement(sql);
      /** 回填參數至SQL指令 */
      prestat.setString(1, id);

      /** 執行查詢之SQL指令並記錄回傳資料 */
      rs = prestat.executeQuery();

      /** 以while迴圈取得下一筆資料 */
      while (rs.next()) {
        /** 取出ResultSet的資料 */
        int visaId = rs.getInt("id");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date createdAt = rs.getTimestamp("created_at");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date updatedAt = rs.getTimestamp("updated_at");
        int accountId = rs.getInt("account_id");
        String bankId = rs.getString("bank_id");
        String cardId = rs.getString("card_id");
        String validationNum = rs.getString("validation_num");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date goodThru = rs.getDate("good_thru");
        boolean deleted = rs.getBoolean("deleted");

        /** 將每一筆資料建立一個新的Visa物件 */
        visa = new Visa(visaId, createdAt, updatedAt, accountId, bankId, cardId, validationNum, goodThru, deleted);
        /** 取出信用卡資料並封裝至JSONArray */
        jsa.put(visa.getData());
      }
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(rs, prestat, conn);
    }

    /** 將信用卡資料的JSONArray封裝成JSONObject回傳 */
    JSONObject response = new JSONObject();
    response.put("data", jsa);

    return response;
  }

  /**
   * 透過帳號編號取得該帳號的信用卡資料
   *
   * @param id 帳號編號
   * @return JSONObject 該帳號的信用卡資料
   */
  public JSONObject getByAccountId(String id) {
    /** 新建Visa物件之visa變數，用以儲存查詢後的信用卡資料 */
    Visa visa = null;
    /** 儲存所有查詢到的信用卡資料，以JSONArray的方式儲存 */
    JSONArray jsa = new JSONArray();
    /** 儲存所有JDBC檢索後的結果，以pointer方式讀取 */
    ResultSet rs = null;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      /** SQL指令 */
      String sql = "SELECT * FROM `visas` WHERE `account_id` = ?";

      /** 預編譯SQL指令 */
      prestat = conn.prepareStatement(sql);
      /** 回填參數至SQL指令 */
      prestat.setString(1, id);

      /** 執行查詢之SQL指令並記錄回傳資料 */
      rs = prestat.executeQuery();

      /** 以while迴圈取得下一筆資料 */
      while (rs.next()) {
        /** 取出ResultSet的資料 */
        int visaId = rs.getInt("id");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date createdAt = rs.getTimestamp("created_at");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date updatedAt = rs.getTimestamp("updated_at");
        int accountId = rs.getInt("account_id");
        String bankId = rs.getString("bank_id");
        String cardId = rs.getString("card_id");
        String validationNum = rs.getString("validation_num");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date goodThru = rs.getDate("good_thru");
        boolean deleted = rs.getBoolean("deleted");

        /** 將每一筆資料建立一個新的Visa物件 */
        visa = new Visa(visaId, createdAt, updatedAt, accountId, bankId, cardId, validationNum, goodThru, deleted);
        /** 取出信用卡資料並封裝至JSONArray */
        jsa.put(visa.getData());
      }
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(rs, prestat, conn);
    }

    /** 將信用卡資料的JSONArray封裝成JSONObject回傳 */
    JSONObject response = new JSONObject();
    response.put("data", jsa);

    return response;
  }

  /**
   * 檢查信用卡地址是否重複
   *
   * @param visa Visa物件 是檢查資料的Visa物件
   * @return boolean 是否重複
   */
  public boolean checkDuplicate(Visa visa) {
    /** 儲存所有JDBC檢索後的結果，以pointer方式讀取 */
    ResultSet rs = null;
    /** 紀錄SQL結果總行數 */
    int row = 0;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      if (visa.getId() > 0) {
        /** SQL指令 */
        String sql = "SELECT COUNT(*) FROM `visas` WHERE `id` != ? && `card_id` = ?";

        /** 預編譯SQL指令 */
        prestat = conn.prepareStatement(sql);
        /** 回填參數至SQL指令 */
        prestat.setInt(1, visa.getId());
        prestat.setString(2, visa.getCardId());
      } else {
        /** SQL指令 */
        String sql = "SELECT COUNT(*) FROM `visas` WHERE `card_id` = ?";

        /** 預編譯SQL指令 */
        prestat = conn.prepareStatement(sql);
        /** 回填參數至SQL指令 */
        prestat.setString(1, visa.getCardId());
      }

      /** 執行查詢之SQL指令並記錄回傳資料 */
      rs = prestat.executeQuery();

      /** 將pointer移至最後 */
      rs.last();
      /** 取出ResultSet的資料 */
      row = rs.getInt("COUNT(*)");
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(rs, prestat, conn);
    }

    return (row > 0) ? true : false;
  }

  /**
   * 新增信用卡資料
   *
   * @param visa 新增資料的Visa物件
   * @return boolean 是否新增成功
   */
  public boolean create(Visa visa) {
    /** 紀錄SQL結果總行數 */
    int row = 0;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      /** SQL指令 */
      String sql = "INSERT INTO `visas`(`account_id`, `bank_id`, `card_id`, `validation_num`, `good_thru`) VALUES(?, ?, ?, ?, ?)";

      /** 預編譯SQL指令 */
      prestat = conn.prepareStatement(sql);
      /** 回填參數至SQL指令 */
      prestat.setInt(1, visa.getAccountId());
      prestat.setString(2, visa.getBankId());
      prestat.setString(3, visa.getCardId());
      prestat.setString(4, visa.getValidationNum());
      /** 轉換成字串格式輸入資料庫 */
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      prestat.setString(5, sdf.format(visa.getGoodThru()));

      /** 執行更新之SQL指令並記錄回傳資料 */
      row = prestat.executeUpdate();
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(prestat, conn);
    }

    return (row > 0) ? true : false;
  }

  /**
   * 更新信用卡資料
   *
   * @param visa 更新資料的Visa物件
   * @return boolean 是否更新成功
   */
  public boolean update(Visa visa) {
    /** 紀錄SQL結果總行數 */
    int row = 0;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      /** SQL指令 */
      String sql = "UPDATE `visas` SET `bank_id` = ?, `card_id` = ?, `validation_num` = ?, `good_thru` = ? WHERE `id` = ?";

      /** 預編譯SQL指令 */
      prestat = conn.prepareStatement(sql);
      /** 回填參數至SQL指令 */
      prestat.setString(1, visa.getBankId());
      prestat.setString(2, visa.getCardId());
      prestat.setString(3, visa.getValidationNum());
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      prestat.setString(4, sdf.format(visa.getGoodThru()));
      prestat.setInt(5, visa.getId());

      /** 執行更新之SQL指令並記錄回傳資料 */
      row = prestat.executeUpdate();
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(prestat, conn);
    }

    return (row > 0) ? true : false;
  }

  /**
   * 根據信用卡編號刪除或軟刪除信用卡資料
   *
   * @param id 信用卡編號
   * @return boolean 刪除是否成功
   */
  public boolean deleteById(String id) {
    /** 紀錄SQL結果總行數 */
    int row = 0;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      /** SQL指令 */
      String sql = "UPDATE `visas` SET `deleted` = true WHERE `id` = ?";

      /** 預編譯SQL指令 */
      prestat = conn.prepareStatement(sql);
      /** 回填參數至SQL指令 */
      prestat.setString(1, id);

      /** 執行更新之SQL指令並記錄回傳資料 */
      row = prestat.executeUpdate();
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(prestat, conn);
    }

    return (row > 0) ? true : false;
  }
}
