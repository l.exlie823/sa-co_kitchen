package app;

import java.util.*;

import org.json.JSONObject;

public class Kitchen {
  /** 廚房編號 */
  private int id;
  /** 建立時間 */
  private Date createdAt;
  /** 更新時間 */
  private Date updatedAt;
  /** 廚房地址 */
  private String address;
  /** 每小時價錢 */
  private int price;
  /** 軟刪除 */
  private boolean deleted;

  /**
   * Constructor<br>
   * 提供建立新廚房資訊使用
   *
   * @param address 地址
   * @param price   每小時價錢
   */
  public Kitchen(String address, int price) {
    this.address = address;
    this.price = price;
  }

  /**
   * Constructor<br>
   * 提供更新廚房資料使用
   *
   * @param id      廚房編號
   * @param address 地址
   * @param price   每小時價錢
   */
  public Kitchen(int id, String address, int price) {
    this(address, price);
    this.id = id;
  }

  /**
   * Constructor<br>
   * 提供紀錄從資料庫取出的資料使用
   *
   * @param id        廚房編號
   * @param createdAt 建立時間
   * @param updatedAt 更新時間
   * @param address   地址
   * @param price     每小時價錢
   * @param deleted   軟刪除
   */
  public Kitchen(int id, Date createdAt, Date updatedAt, String address, int price, boolean deleted) {
    this(id, address, price);
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deleted = deleted;
  }

  /**
   * 取得廚房編號
   *
   * @return int 廚房編號
   */
  public int getId() {
    return this.id;
  }

  /**
   * 取得建立時間
   *
   * @return Date 建立時間
   */
  public Date getCreatedAt() {
    return this.createdAt;
  }

  /**
   * 取得更新時間
   *
   * @return Date 建立時間
   */
  public Date getUpdatedAt() {
    return this.updatedAt;
  }

  /**
   * 取得廚房地址
   *
   * @return String 廚房地址
   */
  public String getAddress() {
    return this.address;
  }

  /**
   * 取得每小時價錢
   *
   * @return int 每小時價錢
   */
  public int getPrice() {
    return this.price;
  }

  /**
   * 取得是否軟刪除
   *
   * @return boolean 是否軟刪除
   */
  public boolean getDeleted() {
    return this.deleted;
  }

  /**
   * 取得JSONObject格式的廚房資料
   *
   * @return JSONObject 廚房資料
   */
  public JSONObject getData() {
    JSONObject jso = new JSONObject();

    jso.put("id", getId());
    jso.put("createdAt", getCreatedAt());
    jso.put("updatedAt", getUpdatedAt());
    jso.put("address", getAddress());
    jso.put("price", getPrice());
    jso.put("deleted", getDeleted());

    return jso;
  }
}
