package app;

import org.json.*;

import java.sql.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import util.DBMgr;

public class AccountHelper {
  /** 靜態變數，儲存AccountHelper物件 */
  private static AccountHelper ah;
  /** 儲存JDBC資料庫連線 */
  private Connection conn = null;
  /** 儲存JDBC預準備之SQL指令 */
  private PreparedStatement prestat = null;

  /**
   * Constructor<br>
   * 採用Singleton，確保物件唯一
   */
  private AccountHelper() {
  }

  /**
   * 取得AccountHelper物件
   *
   * @return AccountHelper AccountHelper物件
   */
  public static AccountHelper getHelper() {
    /** 檢查是否存在AccountHelper物件，不存在則建立一個 */
    if (ah == null) {
      ah = new AccountHelper();
    }

    /** 回傳AccountHelper物件 */
    return ah;
  }

  /**
   * 取得所有帳號資料
   *
   * @return JSONObject 所有帳號資料
   */
  public JSONObject getAll() {
    /** 新建Account物件之account變數，用以儲存查詢後的帳號資料 */
    Account account = null;
    /** 儲存所有查詢到的帳號資料，以JSONArray的方式儲存 */
    JSONArray jsa = new JSONArray();
    /** 儲存所有JDBC檢索後的結果，以pointer方式讀取 */
    ResultSet rs = null;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      /** SQL指令 */
      String sql = "SELECT * FROM `accounts`";

      /** 預編譯SQL指令 */
      prestat = conn.prepareStatement(sql);
      /** 執行查詢之SQL指令並記錄回傳資料 */
      rs = prestat.executeQuery();

      /** 以while迴圈取得下一筆資料 */
      while (rs.next()) {
        /** 取出ResultSet的資料 */
        int id = rs.getInt("id");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date createdAt = rs.getTimestamp("created_at");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date updatedAt = rs.getTimestamp("updated_at");
        String firstName = rs.getString("first_name");
        String lastName = rs.getString("last_name");
        String email = rs.getString("email");
        String password = rs.getString("password");
        int admin = rs.getInt("admin");
        boolean deleted = rs.getBoolean("deleted");

        /** 將每一筆資料建立一個新的Account物件 */
        account = new Account(id, createdAt, updatedAt, firstName, lastName, email, password, admin, deleted);
        /** 取出帳號資料並封裝至JSONArray */
        jsa.put(account.getData());
      }
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(rs, prestat, conn);
    }

    /** 將所有帳號資料的JSONArray封裝成JSONObject回傳 */
    JSONObject response = new JSONObject();
    response.put("data", jsa);

    return response;
  }

  /**
   * 透過帳號編號取得帳號資料
   *
   * @param id 帳號編號
   * @return JSONObject 帳號資料
   */
  public JSONObject getById(String id) {
    /** 新建Account物件之account變數，用以儲存查詢後的帳號資料 */
    Account account = null;
    /** 儲存所有查詢到的帳號資料，以JSONArray的方式儲存 */
    JSONArray jsa = new JSONArray();
    /** 儲存所有JDBC檢索後的結果，以pointer方式讀取 */
    ResultSet rs = null;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      /** SQL指令 */
      String sql = "SELECT * FROM `accounts` WHERE `id` = ?";

      /** 預編譯SQL指令 */
      prestat = conn.prepareStatement(sql);
      /** 回填參數至SQL指令 */
      prestat.setString(1, id);

      /** 執行查詢之SQL指令並記錄回傳資料 */
      rs = prestat.executeQuery();

      /** 以while迴圈取得下一筆資料 */
      while (rs.next()) {
        /** 取出ResultSet的資料 */
        int account_id = rs.getInt("id");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date createdAt = rs.getTimestamp("created_at");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date updatedAt = rs.getTimestamp("updated_at");
        String firstName = rs.getString("first_name");
        String lastName = rs.getString("last_name");
        String email = rs.getString("email");
        String password = rs.getString("password");
        int admin = rs.getInt("admin");
        boolean deleted = rs.getBoolean("deleted");

        /** 將每一筆資料建立一個新的Account物件 */
        account = new Account(account_id, createdAt, updatedAt, firstName, lastName, email, password, admin, deleted);
        /** 取得帳戶資料並封裝進JSONArray */
        jsa.put(account.getData());
      }
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(rs, prestat, conn);
    }

    /** 將帳號資料的JSONArray封裝成JSONObject回傳 */
    JSONObject response = new JSONObject();
    response.put("data", jsa);

    return response;
  }

  /**
   * 檢查帳號的email或員工編號是否重複
   *
   * @param account Account物件 受檢查資料的Account物件
   * @return boolean 是否重複
   */
  public boolean checkDuplicate(Account account) {
    /** 儲存所有JDBC檢索後的結果，以pointer方式讀取 */
    ResultSet rs = null;
    /** 紀錄SQL結果總行數 */
    int row = 0;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      if (account.getId() > 0) {
        if (account.getAdmin() > 0) {
          /** SQL指令 */
          String sql = "SELECT COUNT(*) FROM `accounts` WHERE `id` != ? && `email` = ? || `admin` = ?";

          /** 預編譯SQL指令 */
          prestat = conn.prepareStatement(sql);
          /** 回填參數至SQL指令 */
          prestat.setInt(1, account.getId());
          prestat.setString(2, account.getEmail());
          prestat.setInt(3, account.getAdmin());
        } else {
          /** SQL指令 */
          String sql = "SELECT COUNT(*) FROM `accounts` WHERE `id` != ? && `email` = ?";

          /** 預編譯SQL指令 */
          prestat = conn.prepareStatement(sql);
          /** 回填參數至SQL指令 */
          prestat.setInt(1, account.getId());
          prestat.setString(2, account.getEmail());
        }

        /** 執行查詢之SQL指令並記錄回傳資料 */
        rs = prestat.executeQuery();

        /** 將pointer移至最後 */
        rs.last();
        /** 取出ResultSet的資料 */
        row = rs.getInt("COUNT(*)");
      } else {
        if (account.getAdmin() > 0) {
          /** SQL指令 */
          String sql = "SELECT COUNT(*) FROM `accounts` WHERE `email` = ? || `admin` = ?";

          /** 預編譯SQL指令 */
          prestat = conn.prepareStatement(sql);
          /** 回填參數至SQL指令 */
          prestat.setString(1, account.getEmail());
          prestat.setInt(2, account.getAdmin());
        } else {
          /** SQL指令 */
          String sql = "SELECT COUNT(*) FROM `accounts` WHERE `email` = ?";

          /** 預編譯SQL指令 */
          prestat = conn.prepareStatement(sql);
          /** 回填參數至SQL指令 */
          prestat.setString(1, account.getEmail());
        }

        /** 執行查詢之SQL指令並記錄回傳資料 */
        rs = prestat.executeQuery();

        /** 將pointer移至最後 */
        rs.last();
        /** 取出ResultSet的資料 */
        row = rs.getInt("COUNT(*)");
      }
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(rs, prestat, conn);
    }

    return (row > 0) ? true : false;
  }

  /**
   * 新增資料
   *
   * @param account 新增資料的Account物件
   * @return boolean 是否新增成功
   */
  public boolean create(Account account) {
    /** 紀錄SQL結果總行數 */
    int row = 0;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      /** 若遇新增管理員則多輸入員工編號 */
      if (account.getAdmin() > 0) {
        /** SQL指令 */
        String sql = "INSERT INTO `accounts`(`first_name`, `last_name`, `email`, `password`, `admin`) VALUES(?, ?, ?, ?, ?)";

        /** 預編譯SQL指令 */
        prestat = conn.prepareStatement(sql);
        /** 回填參數至SQL指令 */
        prestat.setString(1, account.getFirstName());
        prestat.setString(2, account.getLastName());
        prestat.setString(3, account.getEmail());
        prestat.setString(4, account.getPassword());
        prestat.setInt(5, account.getAdmin());
      } else {
        /** SQL指令 */
        String sql = "INSERT INTO `accounts`(`first_name`, `last_name`, `email`, `password`) VALUES(?, ?, ?, ?)";

        /** 預編譯SQL指令 */
        prestat = conn.prepareStatement(sql);
        /** 回填參數至SQL指令 */
        prestat.setString(1, account.getFirstName());
        prestat.setString(2, account.getLastName());
        prestat.setString(3, account.getEmail());
        prestat.setString(4, account.getPassword());
      }

      /** 執行更新之SQL指令並記錄回傳資料 */
      row = prestat.executeUpdate();
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(prestat, conn);
    }

    return (row > 0) ? true : false;
  }

  /**
   * 更新帳號資料
   *
   * @param account 更新資料的Account物件
   * @return boolean 是否更新成功
   */
  public boolean update(Account account) {
    ResultSet rs = null;
    /** 紀錄SQL結果總行數 */
    int row = 0;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      /** SQL指令 */
      String sql = "SELECT deleted FROM `accounts` WHERE `id` = ?";

      /** 預編譯SQL指令 */
      prestat = conn.prepareStatement(sql);
      /** 回填參數至SQL指令 */
      prestat.setInt(1, account.getId());

      /** 執行查詢之SQL指令並記錄回傳資料 */
      rs = prestat.executeQuery();

      rs.first();

      /** 檢查是否已軟刪除，若無則進行修改資料，反之回報錯誤訊息 */
      if (!rs.getBoolean("deleted")) {
        if (account.getAdmin() > 0) {
          /** SQL指令 */
          sql = "UPDATE `accounts` SET `first_name` = ?, `last_name` = ?, `email` = ?, `password` = ?, `admin` = ? WHERE `id` = ?";

          /** 預編譯SQL指令 */
          prestat = conn.prepareStatement(sql);
          /** 回填參數至SQL指令 */
          prestat.setString(1, account.getFirstName());
          prestat.setString(2, account.getLastName());
          prestat.setString(3, account.getEmail());
          prestat.setString(4, account.getPassword());
          prestat.setInt(5, account.getAdmin());
          prestat.setInt(6, account.getId());
        } else {
          /** SQL指令 */
          sql = "UPDATE `accounts` SET `first_name` = ?, `last_name` = ?, `email` = ?, `password` = ? WHERE `id` = ?";

          /** 預編譯SQL指令 */
          prestat = conn.prepareStatement(sql);
          /** 回填參數至SQL指令 */
          prestat.setString(1, account.getFirstName());
          prestat.setString(2, account.getLastName());
          prestat.setString(3, account.getEmail());
          prestat.setString(4, account.getPassword());
          prestat.setInt(5, account.getId());
        }
        /** 執行更新之SQL指令並記錄回傳資料 */
        row = prestat.executeUpdate();
      }
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(rs, prestat, conn);
    }

    return (row > 0) ? true : false;
  }

  /**
   * 根據帳號編號刪除帳號資料
   *
   * @param id 帳號編號
   * @return boolean 刪除是否成功
   */
  public boolean deleteById(String id) {
    /** 紀錄SQL結果總行數 */
    int row = 0;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      /** SQL指令 */
      String sql = "UPDATE `accounts` SET `deleted` = true WHERE `id` = ?";

      /** 預編譯SQL指令 */
      prestat = conn.prepareStatement(sql);
      /** 回填參數至SQL指令 */
      prestat.setString(1, id);

      /** 執行更新之SQL指令並記錄回傳資料 */
      row = prestat.executeUpdate();
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(prestat, conn);
    }

    return (row > 0) ? true : false;
  }

  /**
   * 登入嘗試，也可做刷新session使用<br>
   * 若帳號正確嘗試登入
   *
   * @param account
   * @param request
   * @return
   */
  public boolean loginAttempt(Account account, HttpServletRequest request) {
    /** 儲存所有JDBC檢索後的結果，以pointer方式讀取 */
    ResultSet rs = null;
    /** 儲存所有查詢到的帳號資料 */
    Account a = null;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      /** SQL指令 */
      String sql = "SELECT * FROM `accounts` WHERE `email` = ? && `password` = ? && `deleted` = false";

      /** 預編譯SQL指令 */
      prestat = conn.prepareStatement(sql);
      /** 回填參數至SQL指令 */
      prestat.setString(1, account.getEmail());
      prestat.setString(2, account.getPassword());

      /** 執行查詢之SQL指令並記錄回傳資料 */
      rs = prestat.executeQuery();

      if (rs.next()) {
        /** 取出ResultSet的資料 */
        int id = rs.getInt("id");
        String firstName = rs.getString("first_name");
        String lastName = rs.getString("last_name");
        String email = rs.getString("email");
        String password = rs.getString("password");
        int admin = rs.getInt("admin");

        a = new Account(id, firstName, lastName, email, password, admin);
      }
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(rs, prestat, conn);
    }

    /** 建立登入session */
    if (a != null) {
      request.getSession().setAttribute("id", a.getId());
      request.getSession().setAttribute("name", a.getLastName() + " " + a.getFirstName());
      request.getSession().setAttribute("email", a.getEmail());
      request.getSession().setAttribute("admin", a.getAdmin());

      return true;
    } else {
      return false;
    }
  }

  /**
   * 取得登入中的帳號管理員身分
   *
   * @param request
   * @return
   */
  public boolean checkAdmin(HttpServletRequest request) {
    /** 取得session */
    HttpSession session = request.getSession(false);

    /** 回傳管理員狀態 */
    return session != null && (int) session.getAttribute("admin") > 0 ? true : false;
  }

  /**
   * 取得登入中的帳號id<br>
   * 0表示未登入，其餘表示登入中的帳號id
   *
   * @param request
   * @return 登入中的帳號id
   */
  public int getLoginId(HttpServletRequest request) {
    /** 取得session */
    HttpSession session = request.getSession(false);

    /** 回傳登入狀態 */
    return session == null ? 0 : (int) session.getAttribute("id");
  }
}
