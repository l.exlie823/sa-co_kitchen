package app;

import java.sql.*;
import java.text.SimpleDateFormat;

import org.json.*;

import util.DBMgr;

public class RentHelper {
  /** 靜態變數，儲存RentHelper物件 */
  private static RentHelper rh;
  /** 儲存JDBC資料庫連線 */
  private Connection conn = null;
  /** 儲存JDBC預準備之SQL指令 */
  private PreparedStatement prestat = null;
  /** 日期格式轉換物件 */
  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

  /**
   * Constructor<br>
   * 採用Singleton，確保物件唯一
   */
  private RentHelper() {
  }

  /**
   * 取得RentHelper物件
   *
   * @return RentHelper RentHelper物件
   */
  public static RentHelper getHelper() {
    /** 檢查是否存在RentHelper物件，不存在則建立一個 */
    if (rh == null) {
      rh = new RentHelper();
    }

    /** 回傳RentHelper物件 */
    return rh;
  }

  /**
   * 取得所有租用資料
   *
   * @return JSONObject 所有租用資料
   */
  public JSONObject getAll() {
    /** 新建Rent物件之rent變數，用以儲存查詢後的租用資料 */
    Rent rent = null;
    /** 儲存所有查詢到的租用資料，以JSONArray的方式儲存 */
    JSONArray jsa = new JSONArray();
    /** 儲存所有JDBC檢索後的結果，以pointer方式讀取 */
    ResultSet rs = null;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      /** SQL指令 */
      String sql = "SELECT * FROM `rents`";

      /** 預編譯SQL指令 */
      prestat = conn.prepareStatement(sql);
      /** 執行查詢之SQL指令並記錄回傳資料 */
      rs = prestat.executeQuery();

      /** 以while迴圈取得下一筆資料 */
      while (rs.next()) {
        /** 取出ResultSet的資料 */
        int id = rs.getInt("id");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date createdAt = rs.getTimestamp("created_at");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date updatedAt = rs.getTimestamp("updated_at");
        int accountId = rs.getInt("account_id");
        int kitchenId = rs.getInt("kitchen_id");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date startAt = rs.getTimestamp("start_at");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date endAt = rs.getTimestamp("end_at");
        int price = rs.getInt("price");
        boolean paid = rs.getBoolean("paid");

        /** 將每一筆資料建立一個新的Rent物件 */
        rent = new Rent(id, createdAt, updatedAt, accountId, kitchenId, startAt, endAt, price, paid);
        /** 取出租用資料並封裝至JSONArray */
        jsa.put(rent.getData());
      }
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(rs, prestat, conn);
    }

    /** 將所有租用資料的JSONArray封裝成JSONObject回傳 */
    JSONObject response = new JSONObject();
    response.put("data", jsa);

    return response;
  }

  /**
   * 透過租用編號取得租用資料
   *
   * @param id 租用編號
   * @return JSONObject 租用資料
   */
  public JSONObject getById(String id) {
    /** 新建Rent物件之rent變數，用以儲存查詢後的租用資料 */
    Rent rent = null;
    /** 儲存所有查詢到的租用資料，以JSONArray的方式儲存 */
    JSONArray jsa = new JSONArray();
    /** 儲存所有JDBC檢索後的結果，以pointer方式讀取 */
    ResultSet rs = null;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      /** SQL指令 */
      String sql = "SELECT * FROM `rents` WHERE `id` = ?";

      /** 預編譯SQL指令 */
      prestat = conn.prepareStatement(sql);
      /** 回填參數至SQL指令 */
      prestat.setString(1, id);

      /** 執行查詢之SQL指令並記錄回傳資料 */
      rs = prestat.executeQuery();

      /** 以while迴圈取得下一筆資料 */
      while (rs.next()) {
        /** 取出ResultSet的資料 */
        int rentId = rs.getInt("id");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date createdAt = rs.getTimestamp("created_at");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date updatedAt = rs.getTimestamp("updated_at");
        int accountId = rs.getInt("account_id");
        int kitchenId = rs.getInt("kitchen_id");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date startAt = rs.getTimestamp("start_at");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date endAt = rs.getTimestamp("end_at");
        int price = rs.getInt("price");
        boolean paid = rs.getBoolean("paid");

        /** 將每一筆資料建立一個新的Rent物件 */
        rent = new Rent(rentId, createdAt, updatedAt, accountId, kitchenId, startAt, endAt, price, paid);
        /** 取出租用資料並封裝至JSONArray */
        jsa.put(rent.getData());
      }
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(rs, prestat, conn);
    }

    /** 將租用資料的JSONArray封裝成JSONObject回傳 */
    JSONObject response = new JSONObject();
    response.put("data", jsa);

    return response;
  }

  /**
   * 透過帳號編號取得該帳號的租用資料
   *
   * @param id 帳號編號
   * @return JSONObject 該帳號的租用資料
   */
  public JSONObject getByAccountId(String id) {
    /** 新建Rent物件之rent變數，用以儲存查詢後的租用資料 */
    Rent rent = null;
    /** 儲存所有查詢到的租用資料，以JSONArray的方式儲存 */
    JSONArray jsa = new JSONArray();
    /** 儲存所有JDBC檢索後的結果，以pointer方式讀取 */
    ResultSet rs = null;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      /** SQL指令 */
      String sql = "SELECT * FROM `rents` WHERE `account_id` = ?";

      /** 預編譯SQL指令 */
      prestat = conn.prepareStatement(sql);
      /** 回填參數至SQL指令 */
      prestat.setString(1, id);

      /** 執行查詢之SQL指令並記錄回傳資料 */
      rs = prestat.executeQuery();

      /** 以while迴圈取得下一筆資料 */
      while (rs.next()) {
        /** 取出ResultSet的資料 */
        int rentId = rs.getInt("id");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date createdAt = rs.getTimestamp("created_at");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date updatedAt = rs.getTimestamp("updated_at");
        int accountId = rs.getInt("account_id");
        int kitchenId = rs.getInt("kitchen_id");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date startAt = rs.getDate("start_at");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date endAt = rs.getDate("end_at");
        int price = rs.getInt("price");
        boolean paid = rs.getBoolean("paid");

        /** 將每一筆資料建立一個新的Rent物件 */
        rent = new Rent(rentId, createdAt, updatedAt, accountId, kitchenId, startAt, endAt, price, paid);
        /** 取出租用資料並封裝至JSONArray */
        jsa.put(rent.getData());
      }
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(rs, prestat, conn);
    }

    /** 將租用資料的JSONArray封裝成JSONObject回傳 */
    JSONObject response = new JSONObject();
    response.put("data", jsa);

    return response;
  }

  /**
   * 透過廚房編號取得該廚房的租用資料
   *
   * @param id 廚房編號
   * @return JSONObject 該廚房的租用資料
   */
  public JSONObject getByKitchenId(String id) {
    /** 新建Rent物件之rent變數，用以儲存查詢後的租用資料 */
    Rent rent = null;
    /** 儲存所有查詢到的租用資料，以JSONArray的方式儲存 */
    JSONArray jsa = new JSONArray();
    /** 儲存所有JDBC檢索後的結果，以pointer方式讀取 */
    ResultSet rs = null;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      /** SQL指令 */
      String sql = "SELECT * FROM `rents` WHERE `kitchen_id` = ?";

      /** 預編譯SQL指令 */
      prestat = conn.prepareStatement(sql);
      /** 回填參數至SQL指令 */
      prestat.setString(1, id);

      /** 執行查詢之SQL指令並記錄回傳資料 */
      rs = prestat.executeQuery();

      /** 以while迴圈取得下一筆資料 */
      while (rs.next()) {
        /** 取出ResultSet的資料 */
        int rentId = rs.getInt("id");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date createdAt = rs.getTimestamp("created_at");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date updatedAt = rs.getTimestamp("updated_at");
        int accountId = rs.getInt("account_id");
        int kitchenId = rs.getInt("kitchen_id");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date startAt = rs.getDate("start_at");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date endAt = rs.getDate("end_at");
        int price = rs.getInt("price");
        boolean paid = rs.getBoolean("paid");

        /** 將每一筆資料建立一個新的Rent物件 */
        rent = new Rent(rentId, createdAt, updatedAt, accountId, kitchenId, startAt, endAt, price, paid);
        /** 取出租用資料並封裝至JSONArray */
        jsa.put(rent.getData());
      }
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(rs, prestat, conn);
    }

    /** 將租用資料的JSONArray封裝成JSONObject回傳 */
    JSONObject response = new JSONObject();
    response.put("data", jsa);

    return response;
  }

  /**
   * 檢查租用時間是否重複
   *
   * @param rent Rent物件 是檢查資料的Rent物件
   * @return boolean 是否重複
   */
  public boolean checkDuplicate(Rent rent) {
    /** 儲存所有JDBC檢索後的結果，以pointer方式讀取 */
    ResultSet rs = null;
    /** 紀錄SQL結果總行數 */
    int row = 0;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      if (rent.getId() > 0) {
        /** SQL指令 */
        String sql = "SELECT COUNT(*) FROM `rents` WHERE `id` != ? && `kitchen_id` = ? && `end_at` > ? && `start_at` < ?";

        /** 預編譯SQL指令 */
        prestat = conn.prepareStatement(sql);
        /** 回填參數至SQL指令 */
        prestat.setInt(1, rent.getId());
        prestat.setInt(2, rent.getKitchenId());
        prestat.setString(3, sdf.format(rent.getStartAt()));
        prestat.setString(4, sdf.format(rent.getEndAt()));
      } else {
        /** SQL指令 */
        String sql = "SELECT COUNT(*) FROM `rents` WHERE `kitchen_id` = ? && `end_at` > ? && `start_at` < ?";

        /** 預編譯SQL指令 */
        prestat = conn.prepareStatement(sql);
        /** 回填參數至SQL指令 */
        prestat.setInt(1, rent.getKitchenId());
        prestat.setString(2, sdf.format(rent.getStartAt()));
        prestat.setString(3, sdf.format(rent.getEndAt()));
      }

      /** 執行查詢之SQL指令並記錄回傳資料 */
      rs = prestat.executeQuery();

      /** 將pointer移至最後 */
      rs.last();
      /** 取出ResultSet的資料 */
      row = rs.getInt("COUNT(*)");
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(rs, prestat, conn);
    }

    return (row > 0) ? true : false;
  }

  /**
   * 新增租用資料
   *
   * @param rent 新增資料的Rent物件
   * @return boolean 是否新增成功
   */
  public boolean create(Rent rent) {
    /** 紀錄SQL結果總行數 */
    int row = 0;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      /** SQL指令 */
      String sql = "INSERT INTO `rents`(`account_id`, `kitchen_id`, `start_at`, `end_at`, `price`) VALUES(?, ?, ?, ?, ?)";

      /** 預編譯SQL指令 */
      prestat = conn.prepareStatement(sql);
      /** 回填參數至SQL指令 */
      prestat.setInt(1, rent.getAccountId());
      prestat.setInt(2, rent.getKitchenId());
      prestat.setString(3, sdf.format(rent.getStartAt()));
      prestat.setString(4, sdf.format(rent.getEndAt()));
      /** 轉換成字串格式輸入資料庫 */
      prestat.setInt(5, rent.getPrice());

      /** 執行更新之SQL指令並記錄回傳資料 */
      row = prestat.executeUpdate();
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(prestat, conn);
    }

    return (row > 0) ? true : false;
  }

  /**
   * 更新租用資料
   *
   * @param rent 更新資料的Rent物件
   * @return boolean 是否更新成功
   */
  public boolean update(Rent rent) {
    /** 紀錄SQL結果總行數 */
    int row = 0;

    JSONObject a = this.getById(String.valueOf(rent.getId())).getJSONArray("data").getJSONObject(0);

    if (a.getBoolean("paid") == false) {
      try {
        /** 取得資料庫連線 */
        conn = DBMgr.getConnection();

        /** SQL指令 */
        String sql = "UPDATE `rents` SET `account_id` = ?, `kitchen_id` = ?, `start_at` = ?, `end_at` = ?, `price` = ? WHERE `id` = ?";

        /** 預編譯SQL指令 */
        prestat = conn.prepareStatement(sql);
        /** 回填參數至SQL指令 */
        prestat.setInt(1, rent.getAccountId());
        prestat.setInt(2, rent.getKitchenId());
        prestat.setString(3, sdf.format(rent.getStartAt()));
        prestat.setString(4, sdf.format(rent.getEndAt()));
        prestat.setInt(5, rent.getPrice());
        prestat.setInt(6, rent.getId());

        /** 執行更新之SQL指令並記錄回傳資料 */
        row = prestat.executeUpdate();
      } catch (SQLException e) {
        /** 印出JDBC SQL指令錯誤 **/
        System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
      } catch (Exception e) {
        /** 若錯誤則印出錯誤訊息 */
        e.printStackTrace();
      } finally {
        /** 關閉連線並釋放所有資料庫相關之資源 **/
        DBMgr.close(prestat, conn);
      }
    }

    return (row > 0) ? true : false;
  }

  /**
   * 根據租用編號刪除或軟刪除租用資料
   *
   * @param id 租用編號
   * @return boolean 刪除是否成功
   */
  public boolean deleteById(String id) {
    /** 紀錄SQL結果總行數 */
    int row = 0;

    JSONObject a = this.getById(id).getJSONArray("data").getJSONObject(0);

    if (a.getBoolean("paid") == false) {
      try {
        /** 取得資料庫連線 */
        conn = DBMgr.getConnection();

        /** SQL指令 */
        String sql = "DELETE FROM `rents` WHERE `id` = ?";

        /** 預編譯SQL指令 */
        prestat = conn.prepareStatement(sql);
        /** 回填參數至SQL指令 */
        prestat.setString(1, id);

        /** 執行更新之SQL指令並記錄回傳資料 */
        row = prestat.executeUpdate();
      } catch (SQLException e) {
        /** 印出JDBC SQL指令錯誤 **/
        System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
      } catch (Exception e) {
        /** 若錯誤則印出錯誤訊息 */
        e.printStackTrace();
      } finally {
        /** 關閉連線並釋放所有資料庫相關之資源 **/
        DBMgr.close(prestat, conn);
      }
    }

    return (row > 0) ? true : false;
  }
}
