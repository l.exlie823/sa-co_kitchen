package app;

import java.sql.*;
import org.json.*;

import util.DBMgr;

public class KitchenHelper {
  /** 靜態變數，儲存KitchenHelper物件 */
  private static KitchenHelper kh;
  /** 儲存JDBC資料庫連線 */
  private Connection conn = null;
  /** 儲存JDBC預準備之SQL指令 */
  private PreparedStatement prestat = null;

  /**
   * Constructor<br>
   * 採用Singleton，確保物件唯一
   */
  private KitchenHelper() {
  }

  /**
   * 取得KitchenHelper物件
   *
   * @return KitchenHelper KitchenHelper物件
   */
  public static KitchenHelper getHelper() {
    /** 檢查是否存在KitchenHelper物件，不存在則建立一個 */
    if (kh == null) {
      kh = new KitchenHelper();
    }

    /** 回傳KitchenHelper物件 */
    return kh;
  }

  /**
   * 取得所有廚房資料
   *
   * @return JSONObject 所有廚房資料
   */
  public JSONObject getAll() {
    /** 新建Kitchen物件之kitchen變數，用以儲存查詢後的信用卡資料 */
    Kitchen kitchen = null;
    /** 儲存所有查詢到的信用卡資料，以JSONArray的方式儲存 */
    JSONArray jsa = new JSONArray();
    /** 儲存所有JDBC檢索後的結果，以pointer方式讀取 */
    ResultSet rs = null;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      /** SQL指令 */
      String sql = "SELECT * FROM `kitchens`";

      /** 預編譯SQL指令 */
      prestat = conn.prepareStatement(sql);
      /** 執行查詢之SQL指令並記錄回傳資料 */
      rs = prestat.executeQuery();

      /** 以while迴圈取得下一筆資料 */
      while (rs.next()) {
        /** 取出ResultSet的資料 */
        int id = rs.getInt("id");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date createdAt = rs.getTimestamp("created_at");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date updatedAt = rs.getTimestamp("updated_at");
        String address = rs.getString("address");
        int price = rs.getInt("price");
        boolean deleted = rs.getBoolean("deleted");

        /** 將每一筆資料建立一個新的Kitchen物件 */
        kitchen = new Kitchen(id, createdAt, updatedAt, address, price, deleted);
        /** 取出廚房資料並封裝至JSONArray */
        jsa.put(kitchen.getData());
      }
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(rs, prestat, conn);
    }

    /** 將所有廚房資料的JSONArray封裝成JSONObject回傳 */
    JSONObject response = new JSONObject();
    response.put("data", jsa);

    return response;
  }

  /**
   * 透過廚房編號取得廚房資料
   *
   * @param id 廚房編號
   * @return JSONObject 廚房資料
   */
  public JSONObject getById(String id) {
    /** 新建Kitchen物件之kitchen變數，用以儲存查詢後的廚房資料 */
    Kitchen kitchen = null;
    /** 儲存所有查詢到的廚房資料，以JSONArray的方式儲存 */
    JSONArray jsa = new JSONArray();
    /** 儲存所有JDBC檢索後的結果，以pointer方式讀取 */
    ResultSet rs = null;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      /** SQL指令 */
      String sql = "SELECT * FROM `kitchens` WHERE `id` = ?";

      /** 預編譯SQL指令 */
      prestat = conn.prepareStatement(sql);
      /** 回填參數至SQL指令 */
      prestat.setString(1, id);

      /** 執行查詢之SQL指令並記錄回傳資料 */
      rs = prestat.executeQuery();

      /** 以while迴圈取得下一筆資料 */
      while (rs.next()) {
        /** 取出ResultSet的資料 */
        int kitchen_id = rs.getInt("id");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date createdAt = rs.getTimestamp("created_at");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date updatedAt = rs.getTimestamp("updated_at");
        String address = rs.getString("address");
        int price = rs.getInt("price");
        boolean deleted = rs.getBoolean("deleted");

        /** 將每一筆資料建立一個新的Kitchen物件 */
        kitchen = new Kitchen(kitchen_id, createdAt, updatedAt, address, price, deleted);
        /** 取得帳戶資料並封裝進JSONArray */
        jsa.put(kitchen.getData());
      }
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(rs, prestat, conn);
    }

    /** 將廚房資料的JSONArray封裝成JSONObject回傳 */
    JSONObject response = new JSONObject();
    response.put("data", jsa);

    return response;
  }

  /**
   * 檢查廚房地址是否重複
   *
   * @param kitchen Kitchen物件 是檢查資料的Kitchen物件
   * @return boolean 是否重複
   */
  public boolean checkDuplicate(Kitchen kitchen) {
    /** 儲存所有JDBC檢索後的結果，以pointer方式讀取 */
    ResultSet rs = null;
    /** 紀錄SQL結果總行數 */
    int row = 0;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      if (kitchen.getId() > 0) {
        /** SQL指令 */
        String sql = "SELECT COUNT(*) FROM `kitchens` WHERE `id` != ? && `address` = ?";

        /** 預編譯SQL指令 */
        prestat = conn.prepareStatement(sql);
        /** 回填參數至SQL指令 */
        prestat.setInt(1, kitchen.getId());
        prestat.setString(2, kitchen.getAddress());
      } else {
        /** SQL指令 */
        String sql = "SELECT COUNT(*) FROM `kitchens` WHERE `address` = ?";

        /** 預編譯SQL指令 */
        prestat = conn.prepareStatement(sql);
        /** 回填參數至SQL指令 */
        prestat.setString(1, kitchen.getAddress());
      }

      /** 執行查詢之SQL指令並記錄回傳資料 */
      rs = prestat.executeQuery();

      /** 將pointer移至最後 */
      rs.last();
      /** 取出ResultSet的資料 */
      row = rs.getInt("COUNT(*)");
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(rs, prestat, conn);
    }

    return (row > 0) ? true : false;
  }

  /**
   * 新增廚房資料
   *
   * @param kitchen 新增資料的Kitchen物件
   * @return boolean 是否新增成功
   */
  public boolean create(Kitchen kitchen) {
    /** 紀錄SQL結果總行數 */
    int row = 0;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      /** SQL指令 */
      String sql = "INSERT INTO `kitchens`(`address`, `price`) VALUES(?, ?)";

      /** 預編譯SQL指令 */
      prestat = conn.prepareStatement(sql);
      /** 回填參數至SQL指令 */
      prestat.setString(1, kitchen.getAddress());
      prestat.setInt(2, kitchen.getPrice());

      /** 執行更新之SQL指令並記錄回傳資料 */
      row = prestat.executeUpdate();
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(prestat, conn);
    }

    return (row > 0) ? true : false;
  }

  // if deleted cant update
  /**
   * 更新廚房資料
   *
   * @param kitchen 更新資料的Kitchen物件
   * @return boolean 是否更新成功
   */
  public boolean update(Kitchen kitchen) {
    /** 紀錄SQL結果總行數 */
    int row = 0;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      /** SQL指令 */
      String sql = "UPDATE `kitchens` SET `address` = ?, `price` = ? WHERE `id` = ?";

      /** 預編譯SQL指令 */
      prestat = conn.prepareStatement(sql);
      /** 回填參數至SQL指令 */
      prestat.setString(1, kitchen.getAddress());
      prestat.setInt(2, kitchen.getPrice());
      prestat.setInt(3, kitchen.getId());

      /** 執行更新之SQL指令並記錄回傳資料 */
      row = prestat.executeUpdate();
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(prestat, conn);
    }

    return (row > 0) ? true : false;
  }

  /**
   * 根據廚房編號刪除或軟刪除廚房資料
   *
   * @param id 廚房編號
   * @return boolean 刪除是否成功
   */
  public boolean deleteById(String id) {
    /** 紀錄SQL結果總行數 */
    int row = 0;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      /** SQL指令 */
      String sql = "UPDATE `kitchens` SET `deleted` = true WHERE `id` = ?";

      /** 預編譯SQL指令 */
      prestat = conn.prepareStatement(sql);
      /** 回填參數至SQL指令 */
      prestat.setString(1, id);

      /** 執行更新之SQL指令並記錄回傳資料 */
      row = prestat.executeUpdate();
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(prestat, conn);
    }

    return (row > 0) ? true : false;
  }
}
