package app;

import java.util.Date;

import org.json.JSONObject;

public class Rent {
  /** 租用編號 */
  private int id;
  /** 建立時間 */
  private Date createdAt;
  /** 更新時間 */
  private Date updatedAt;
  /** 租用者之帳號編號 */
  private int accountId;
  /** 租用之廚房編號 */
  private int kitchenId;
  /** 租用開始時間 */
  private Date startAt;
  /** 租用結束時間 */
  private Date endAt;
  /** 價錢 */
  private int price;
  /** 是否付款 */
  private boolean paid;

  /**
   * Constructor<br>
   * 提供建立新租用資料使用
   *
   * @param accountId 租用者之帳號編號
   * @param kitchenId 租用之廚房編號
   * @param startAt   租用開始時間
   * @param endAt     租用結束時間
   * @param price     價錢
   */
  public Rent(int accountId, int kitchenId, Date startAt, Date endAt, int price) {
    this.accountId = accountId;
    this.kitchenId = kitchenId;
    this.startAt = startAt;
    this.endAt = endAt;
    this.price = price;
  }

  /**
   * Constructor<br>
   * 提供更新租用資料使用
   *
   * @param id        租用編號
   * @param accountId 租用者者帳號編號
   * @param kitchenId 租用之廚房編號
   * @param startAt   租用開始時間
   * @param endAt     租用結束時間
   * @param price     價錢
   */
  public Rent(int id, int accountId, int kitchenId, Date startAt, Date endAt, int price) {
    this(accountId, kitchenId, startAt, endAt, price);
    this.id = id;
  }

  /**
   * Constructor<br>
   * 提供記錄從資料庫取出的資料使用
   *
   * @param id        租用編號
   * @param createdAt 建立時間
   * @param updatedAt 更新時間
   * @param accountId 租用者帳號編號
   * @param kitchenId 租用之廚房編號
   * @param startAt   租用開始時間
   * @param endAt     租用結束時間
   * @param price     價錢
   * @param paid      是否付款
   */
  public Rent(int id, Date createdAt, Date updatedAt, int accountId, int kitchenId, Date startAt, Date endAt, int price,
      boolean paid) {
    this(id, accountId, kitchenId, startAt, endAt, price);
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.paid = paid;
  }

  /**
   * 取得租用編號
   *
   * @return int 租用編號
   */
  public int getId() {
    return this.id;
  }

  /**
   * 取得建立時間
   *
   * @return Date 建立時間
   */
  public Date getCreatedAt() {
    return this.createdAt;
  }

  /**
   * 取得更新時間
   *
   * @return Date 更新時間
   */
  public Date getUpdatedAt() {
    return this.updatedAt;
  }

  /**
   * 取得租用者帳號編號
   *
   * @return int 租用者帳號編號
   */
  public int getAccountId() {
    return this.accountId;
  }

  /**
   * 取得租用之廚房編號
   *
   * @return int 租用之廚房編號
   */
  public int getKitchenId() {
    return this.kitchenId;
  }

  /**
   * 取得租用開始時間
   *
   * @return Date 租用開始時間
   */
  public Date getStartAt() {
    return this.startAt;
  }

  /**
   * 取得租用結束時間
   *
   * @return Date 租用結束時間
   */
  public Date getEndAt() {
    return this.endAt;
  }

  /**
   * 取得價錢
   *
   * @return int 價錢
   */
  public int getPrice() {
    return this.price;
  }

  /**
   * 取得是否付款
   *
   * @return boolean 是否付款
   */
  public boolean getPaid() {
    return this.paid;
  }

  /**
   * 取得JSONObject格式的租用資料
   *
   * @return JSONObject 租用資料
   */
  public JSONObject getData() {
    JSONObject jso = new JSONObject();

    jso.put("id", getId());
    jso.put("createdAt", getCreatedAt());
    jso.put("updatedAt", getUpdatedAt());
    jso.put("accountId", getAccountId());
    jso.put("kitchenId", getKitchenId());
    jso.put("startAt", getStartAt());
    jso.put("endAt", getEndAt());
    jso.put("price", getPrice());
    jso.put("paid", getPaid());

    return jso;
  }
}
