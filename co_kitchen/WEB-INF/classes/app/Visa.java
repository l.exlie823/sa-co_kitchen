package app;

import java.util.Date;

import org.json.JSONObject;

public class Visa {
  /** 信用卡編號 */
  private int id;
  /** 建立時間 */
  private Date createdAt;
  /** 更新時間 */
  private Date updatedAt;
  /** 持有者之帳號編號 */
  private int accountId;
  /** 銀行行號 */
  private String bankId;
  /** 信用卡號 */
  private String cardId;
  /** 驗證碼 */
  private String validationNum;
  /** 有效年月 */
  private Date goodThru;
  /** 是否軟刪除 */
  private boolean deleted;

  /**
   * Constructor<br>
   * 提供建立新信用卡資料使用
   *
   * @param accountId     持有者之帳號編號
   * @param bankId        銀行行號
   * @param cardId        信用卡卡號
   * @param validationNum 驗證碼
   * @param goodThru      有效年月
   */
  public Visa(int accountId, String bankId, String cardId, String validationNum, Date goodThru) {
    this.accountId = accountId;
    this.bankId = bankId;
    this.cardId = cardId;
    this.validationNum = validationNum;
    this.goodThru = goodThru;
  }

  /**
   * Constructor<br>
   * 提供更新信用卡資料使用
   *
   * @param id            信用卡編號
   * @param accountId     持有者帳號編號
   * @param bankId        銀行行號
   * @param cardId        信用卡卡號
   * @param validationNum 驗證碼
   * @param goodThru      有效年月
   */
  public Visa(int id, int accountId, String bankId, String cardId, String validationNum, Date goodThru) {
    this(accountId, bankId, cardId, validationNum, goodThru);
    this.id = id;
  }

  /**
   * Constructor<br>
   * 提供記錄從資料庫取出的資料使用
   *
   * @param id            信用卡編號
   * @param createdAt     建立時間
   * @param updatedAt     更新時間
   * @param accountId     持有者帳號編號
   * @param bankId        銀行行號
   * @param cardId        信用卡卡號
   * @param validationNum 驗證碼
   * @param goodThru      有效年月
   * @param deleted       是否軟刪除
   */
  public Visa(int id, Date createdAt, Date updatedAt, int accountId, String bankId, String cardId, String validationNum,
      Date goodThru, boolean deleted) {
    this(id, accountId, bankId, cardId, validationNum, goodThru);
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deleted = deleted;
  }

  /**
   * 取得信用卡編號
   *
   * @return int 信用卡編號
   */
  public int getId() {
    return this.id;
  }

  /**
   * 取得建立時間
   *
   * @return Date 建立時間
   */
  public Date getCreatedAt() {
    return this.createdAt;
  }

  /**
   * 取得更新時間
   *
   * @return Date 更新時間
   */
  public Date getUpdatedAt() {
    return this.updatedAt;
  }

  /**
   * 取得持有者帳號編號
   *
   * @return int 持有者帳號編號
   */
  public int getAccountId() {
    return this.accountId;
  }

  /**
   * 取得銀行行號
   *
   * @return String 銀行行號
   */
  public String getBankId() {
    return this.bankId;
  }

  /**
   * 取得信用卡卡號
   *
   * @return String 信用卡卡號
   */
  public String getCardId() {
    return this.cardId;
  }

  /**
   * 取得驗證碼
   *
   * @return String 驗證碼
   */
  public String getValidationNum() {
    return this.validationNum;
  }

  /**
   * 取得有效年月
   *
   * @return Date 有效年月
   */
  public Date getGoodThru() {
    return this.goodThru;
  }

  /**
   * 取得是否軟刪除
   *
   * @return boolean 是否軟刪除
   */
  public boolean getDeleted() {
    return this.deleted;
  }

  /**
   * 取得JSONObject格式的信用卡資料
   *
   * @return JSONObject 信用卡資料
   */
  public JSONObject getData() {
    JSONObject jso = new JSONObject();

    jso.put("id", getId());
    jso.put("createdAt", getCreatedAt());
    jso.put("updatedAt", getUpdatedAt());
    jso.put("accountId", getAccountId());
    jso.put("bankId", getBankId());
    jso.put("cardId", getCardId());
    jso.put("validationNum", getValidationNum());
    jso.put("goodThru", getGoodThru());
    jso.put("deleted", getDeleted());

    return jso;
  }
}
