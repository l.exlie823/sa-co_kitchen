package app;

import java.sql.*;

import org.json.*;

import util.*;

public class TransactionHelper {
  /** 靜態變數，儲存TransactionHelper物件 */
  private static TransactionHelper th;
  /** 儲存JDBC資料庫連線 */
  private Connection conn = null;
  /** 儲存JDBC預準備之SQL指令 */
  private PreparedStatement prestat = null;

  /**
   * Constructor<br>
   * 採用Singleton，確保物件唯一
   */
  private TransactionHelper() {
  }

  /**
   * 取得TransactionHelper物件
   *
   * @return TransactionHelper TransactionHelper物件
   */
  public static TransactionHelper getHelper() {
    /** 檢查是否存在TransactionHelper物件，不存在則建立一個 */
    if (th == null) {
      th = new TransactionHelper();
    }

    /** 回傳TransactionHelper物件 */
    return th;
  }

  /**
   * 取得所有交易資料
   *
   * @return JSONObject 所有交易資料
   */
  public JSONObject getAll() {
    /** 新建Transaction物件之transaction變數，用以儲存查詢後的交易資料 */
    Transaction transaction = null;
    /** 儲存所有查詢到的交易資料，以JSONArray的方式儲存 */
    JSONArray jsa = new JSONArray();
    /** 儲存所有JDBC檢索後的結果，以pointer方式讀取 */
    ResultSet rs = null;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      /** SQL指令 */
      String sql = "SELECT * FROM `transactions`";

      /** 預編譯SQL指令 */
      prestat = conn.prepareStatement(sql);
      /** 執行查詢之SQL指令並記錄回傳資料 */
      rs = prestat.executeQuery();

      /** 以while迴圈取得下一筆資料 */
      while (rs.next()) {
        /** 取出ResultSet的資料 */
        int id = rs.getInt("id");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date createdAt = rs.getTimestamp("created_at");
        int accountId = rs.getInt("account_id");
        int rentId = rs.getInt("rent_id");
        int visaId = rs.getInt("visa_id");

        /** 將每一筆資料建立一個新的Transaction物件 */
        transaction = new Transaction(id, createdAt, accountId, rentId, visaId);
        /** 取出交易資料並封裝至JSONArray */
        jsa.put(transaction.getData());
      }
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(rs, prestat, conn);
    }

    /** 將所有交易資料的JSONArray封裝成JSONObject回傳 */
    JSONObject response = new JSONObject();
    response.put("data", jsa);

    return response;
  }

  /**
   * 透過交易編號取得交易資料
   *
   * @param id 交易編號
   * @return JSONObject 交易資料
   */
  public JSONObject getById(String id) {
    /** 新建Transaction物件之transaction變數，用以儲存查詢後的交易資料 */
    Transaction transaction = null;
    /** 儲存所有查詢到的交易資料，以JSONArray的方式儲存 */
    JSONArray jsa = new JSONArray();
    /** 儲存所有JDBC檢索後的結果，以pointer方式讀取 */
    ResultSet rs = null;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      /** SQL指令 */
      String sql = "SELECT * FROM `transactions` WHERE `id` = ?";

      /** 預編譯SQL指令 */
      prestat = conn.prepareStatement(sql);
      /** 回填參數至SQL指令 */
      prestat.setString(1, id);

      /** 執行查詢之SQL指令並記錄回傳資料 */
      rs = prestat.executeQuery();

      /** 以while迴圈取得下一筆資料 */
      while (rs.next()) {
        /** 取出ResultSet的資料 */
        int transactionId = rs.getInt("id");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date createdAt = rs.getTimestamp("created_at");
        int accountId = rs.getInt("account_id");
        int rentId = rs.getInt("rent_id");
        int visaId = rs.getInt("visa_id");

        /** 將每一筆資料建立一個新的Transaction物件 */
        transaction = new Transaction(transactionId, createdAt, accountId, rentId, visaId);
        /** 取出交易資料並封裝至JSONArray */
        jsa.put(transaction.getData());
      }
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(rs, prestat, conn);
    }

    /** 將交易資料的JSONArray封裝成JSONObject回傳 */
    JSONObject response = new JSONObject();
    response.put("data", jsa);

    return response;
  }

  /**
   * 透過帳號編號取得該帳號的交易資料
   *
   * @param id 帳號編號
   * @return JSONObject 該帳號的交易資料
   */
  public JSONObject getByAccountId(String id) {
    /** 新建Transaction物件之transaction變數，用以儲存查詢後的交易資料 */
    Transaction transaction = null;
    /** 儲存所有查詢到的交易資料，以JSONArray的方式儲存 */
    JSONArray jsa = new JSONArray();
    /** 儲存所有JDBC檢索後的結果，以pointer方式讀取 */
    ResultSet rs = null;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      /** SQL指令 */
      String sql = "SELECT * FROM `transactions` WHERE `account_id` = ?";

      /** 預編譯SQL指令 */
      prestat = conn.prepareStatement(sql);
      /** 回填參數至SQL指令 */
      prestat.setString(1, id);

      /** 執行查詢之SQL指令並記錄回傳資料 */
      rs = prestat.executeQuery();

      /** 以while迴圈取得下一筆資料 */
      while (rs.next()) {
        /** 取出ResultSet的資料 */
        int transactionId = rs.getInt("id");
        // 因為java.util與java.sql皆有Date物件，詳細指定之
        java.util.Date createdAt = rs.getTimestamp("created_at");
        int accountId = rs.getInt("account_id");
        int rentId = rs.getInt("rent_id");
        int visaId = rs.getInt("visa_id");

        /** 將每一筆資料建立一個新的Transaction物件 */
        transaction = new Transaction(transactionId, createdAt, accountId, rentId, visaId);
        /** 取出交易資料並封裝至JSONArray */
        jsa.put(transaction.getData());
      }
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(rs, prestat, conn);
    }

    /** 將交易資料的JSONArray封裝成JSONObject回傳 */
    JSONObject response = new JSONObject();
    response.put("data", jsa);

    return response;
  }

  /**
   * 檢查交易是否重複
   *
   * @param transaction Transaction物件 是檢查資料的Transaction物件
   * @return boolean 是否重複
   */
  public boolean checkDuplicate(Transaction transaction) {
    /** 儲存所有JDBC檢索後的結果，以pointer方式讀取 */
    ResultSet rs = null;
    /** 紀錄SQL結果總行數 */
    int row = 0;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      /** SQL指令 */
      String sql = "SELECT COUNT(*) FROM `transactions` WHERE `rent_id` = ?";

      /** 預編譯SQL指令 */
      prestat = conn.prepareStatement(sql);
      /** 回填參數至SQL指令 */
      prestat.setInt(1, transaction.getRentId());

      /** 執行查詢之SQL指令並記錄回傳資料 */
      rs = prestat.executeQuery();

      /** 將pointer移至最後 */
      rs.last();
      /** 取出ResultSet的資料 */
      row = rs.getInt("COUNT(*)");
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(rs, prestat, conn);
    }

    return (row > 0) ? true : false;
  }

  /**
   * 新增交易資料
   *
   * @param transaction 新增資料的Transaction物件
   * @return boolean 是否新增成功
   */
  public boolean create(Transaction transaction) {
    /** 紀錄SQL結果總行數 */
    int row = 0;

    try {
      /** 取得資料庫連線 */
      conn = DBMgr.getConnection();

      /** SQL指令 */
      String sql = "INSERT INTO `transactions`(`account_id`, `rent_id`, `visa_id`) VALUES(?, ?, ?)";

      /** 預編譯SQL指令 */
      prestat = conn.prepareStatement(sql);
      /** 回填參數至SQL指令 */
      prestat.setInt(1, transaction.getAccountId());
      prestat.setInt(2, transaction.getRentId());
      prestat.setInt(3, transaction.getVisaId());

      /** 執行更新之SQL指令並記錄回傳資料 */
      row += prestat.executeUpdate();

      /** SQL指令 */
      sql = "UPDATE `rents` SET `paid` = true WHERE `id` = ?";

      /** 預編譯SQL指令 */
      prestat = conn.prepareStatement(sql);
      /** 回填參數至SQL指令 */
      prestat.setInt(1, transaction.getRentId());

      /** 執行更新之SQL指令並記錄回傳資料 */
      row += prestat.executeUpdate();
    } catch (SQLException e) {
      /** 印出JDBC SQL指令錯誤 **/
      System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
    } catch (Exception e) {
      /** 若錯誤則印出錯誤訊息 */
      e.printStackTrace();
    } finally {
      /** 關閉連線並釋放所有資料庫相關之資源 **/
      DBMgr.close(prestat, conn);
    }

    return (row == 2) ? true : false;
  }
}
