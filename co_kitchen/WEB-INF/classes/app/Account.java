package app;

import java.util.*;

import org.json.JSONObject;

public class Account {
  /** 帳號編號 */
  private int id;
  /** 建立時間 */
  private Date createdAt;
  /** 更新時間 */
  private Date updatedAt;
  /** 名字 */
  private String firstName;
  /** 姓氏 */
  private String lastName;
  /** 電子信箱 */
  private String email;
  /** 密碼 */
  private String password;
  /** 管理員身分，0為無權限，否則填入員工編號 */
  private int admin;
  /** 是否軟刪除 */
  private boolean deleted;

  /**
   * Constructor
   */
  public Account() {
  }

  /**
   * Constructor<br>
   * 提供登入帳號資料使用
   *
   * @param email    電子信箱
   * @param password 密碼
   */
  public Account(String email, String password) {
    this.email = email;
    this.password = password;
  }

  /**
   * Constructor<br>
   * 提供第一次註冊帳號資料使用
   *
   * @param firstName 名字
   * @param lastName  姓氏
   * @param email     電子信箱
   * @param password  密碼
   */
  public Account(String firstName, String lastName, String email, String password) {
    this(email, password);
    this.firstName = firstName;
    this.lastName = lastName;
  }

  /**
   * Constructor<br>
   * 提供第一次新增管理員帳號使用
   *
   * @param firstName 名字
   * @param lastName  姓氏
   * @param email     電子信箱
   * @param password  密碼
   * @param admin     管理者權限(0 or 員工編號)
   */
  public Account(String firstName, String lastName, String email, String password, int admin) {
    this(firstName, lastName, email, password);
    this.admin = admin;
  }

  /**
   * Constructor<br>
   * 提供修改帳號資料使用
   *
   * @param id        帳號編號
   * @param firstName 名字
   * @param lastName  姓氏
   * @param email     電子信箱
   * @param password  密碼
   * @param admin     管理者權限(0 or 員工編號)
   */
  public Account(int id, String firstName, String lastName, String email, String password, int admin) {
    this(firstName, lastName, email, password, admin);
    this.id = id;
  }

  /**
   * Constructor<br>
   * 提供資料庫資料在程式中建立使用
   *
   * @param id        帳號編號
   * @param createdAt 建立時間
   * @param updatedAt 更新時間
   * @param firstName 名字
   * @param lastName  姓氏
   * @param email     電子信箱
   * @param password  密碼
   * @param admin     管理者權限(0 or 員工編號)
   * @param deleted   是否軟刪除
   */
  public Account(int id, Date createdAt, Date updatedAt, String firstName, String lastName, String email,
      String password, int admin, boolean deleted) {
    this(id, firstName, lastName, email, password, admin);
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deleted = deleted;
  }

  /**
   * 取得帳號編號
   *
   * @return int 帳號編號
   */
  public int getId() {
    return this.id;
  }

  /**
   * 取得建立時間
   *
   * @return Date 建立時間
   */
  public Date getCreatedAt() {
    return this.createdAt;
  }

  /**
   * 取得更新時間
   *
   * @return Date 更新時間
   */
  public Date getUpdatedAt() {
    return this.updatedAt;
  }

  /**
   * 取得名字
   *
   * @return String 名字
   */
  public String getFirstName() {
    return this.firstName;
  }

  /**
   * 取得姓氏
   *
   * @return String 姓氏
   */
  public String getLastName() {
    return this.lastName;
  }

  /**
   * 取得電子信箱
   *
   * @return String 電子信箱
   */
  public String getEmail() {
    return this.email;
  }

  /**
   * 取得密碼
   *
   * @return String 密碼
   */
  public String getPassword() {
    return this.password;
  }

  /**
   * 取得管理員權限
   *
   * @return int 管理員權限(0 or 員工編號)
   */
  public int getAdmin() {
    return this.admin;
  }

  /**
   * 取得是否軟刪除
   *
   * @return boolean 是否軟刪除
   */
  public boolean getDeleted() {
    return this.deleted;
  }

  /**
   * 取得Account所有資料
   *
   * @return JSONObject Account資料的json
   */
  public JSONObject getData() {
    JSONObject jso = new JSONObject();

    jso.put("id", getId());
    jso.put("createdAt", getCreatedAt());
    jso.put("updatedAt", getUpdatedAt());
    jso.put("firstName", getFirstName());
    jso.put("lastName", getLastName());
    jso.put("email", getEmail());
    jso.put("password", getPassword());
    jso.put("admin", getAdmin());
    jso.put("deleted", getDeleted());

    return jso;
  }
}
