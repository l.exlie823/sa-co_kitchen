package app;

import java.util.Date;

import org.json.JSONObject;

public class Transaction {
  /** 交易編號 */
  private int id;
  /** 建立時間 */
  private Date createdAt;
  /** 交易者之帳號編號 */
  private int accountId;
  /** 交易之租用編號 */
  private int rentId;
  /** 交易使用之信用卡 */
  private int visaId;

  /**
   * Constructor<br>
   * 提供建立新交易資料使用
   *
   * @param accountId 交易者之帳號編號
   * @param rentId    交易之租用編號
   * @param visaId    交易使用之信用卡
   */
  public Transaction(int accountId, int rentId, int visaId) {
    this.accountId = accountId;
    this.rentId = rentId;
    this.visaId = visaId;
  }

  /**
   * Constructor<br>
   * 提供記錄從資料庫取出的資料使用
   *
   * @param id        交易編號
   * @param createdAt 建立時間
   * @param accountId 交易者帳號編號
   * @param rentId    交易之租用編號
   * @param visaId    交易使用之信用卡
   */
  public Transaction(int id, Date createdAt, int accountId, int rentId, int visaId) {
    this(accountId, rentId, visaId);
    this.id = id;
    this.createdAt = createdAt;
  }

  /**
   * 取得交易編號
   *
   * @return int 交易編號
   */
  public int getId() {
    return this.id;
  }

  /**
   * 取得建立時間
   *
   * @return Date 建立時間
   */
  public Date getCreatedAt() {
    return this.createdAt;
  }

  /**
   * 取得交易者帳號編號
   *
   * @return int 交易者帳號編號
   */
  public int getAccountId() {
    return this.accountId;
  }

  /**
   * 取得交易之租用編號
   *
   * @return int 交易之租用編號
   */
  public int getRentId() {
    return this.rentId;
  }

  /**
   * 取得交易使用之信用卡
   *
   * @return int 交易使用之信用卡
   */
  public int getVisaId() {
    return this.visaId;
  }

  /**
   * 取得JSONObject格式的交易資料
   *
   * @return JSONObject 交易資料
   */
  public JSONObject getData() {
    JSONObject jso = new JSONObject();

    jso.put("id", getId());
    jso.put("createdAt", getCreatedAt());
    jso.put("accountId", getAccountId());
    jso.put("rentId", getRentId());
    jso.put("visaId", getVisaId());

    return jso;
  }
}
