package controllers;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import org.json.JSONObject;

import app.*;
import tools.JsonReader;

public class RentController extends HttpServlet {
  /** 版本號，避免版本不相容而導致error */
  private static final long serialVersionUID = 1L;
  private RentHelper rh = RentHelper.getHelper();
  private AccountHelper ah = AccountHelper.getHelper();

  /**
   * 處理Http Get method<br>
   * 1. 取得所有租用資訊(租用清單、所有租用清單)<br>
   * 2. 取得個別租用資訊(編輯租用)
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    /** 解析傳入的request */
    JsonReader jsr = new JsonReader(request);
    /** 回傳的json */
    JSONObject resp = new JSONObject();
    /** 取得要求的租用編號 */
    String id = jsr.getParameter("id");
    /** 取得要求的帳號編號 */
    String accountId = jsr.getParameter("accountId");
    /** 取得要求的廚房編號 */
    String kitchenId = jsr.getParameter("kitchenId");

    /** 根據不同要求，回傳所有資料/特定租用資料/特定使用者租用資料/特定廚房的租用資料 */
    if (id.isEmpty() && accountId.isEmpty() && kitchenId.isEmpty()) {
      JSONObject result = rh.getAll();

      /** 填入回傳訊息與Query結果 */
      resp.put("message", "查詢成功！");
      resp.put("result", result);
    } else if (!id.isEmpty() && accountId.isEmpty() && kitchenId.isEmpty()) {
      JSONObject result = rh.getById(id);

      if (!result.isEmpty()) {
        /** 填入回傳訊息與Query結果 */
        resp.put("message", "查詢成功！");
        resp.put("result", result);
      } else {
        resp.put("errors", "查詢條件錯誤！");
      }
    } else if (id.isEmpty() && !accountId.isEmpty() && kitchenId.isEmpty()) {
      JSONObject result = rh.getByAccountId(accountId);

      /** 填入回傳訊息與Query結果 */
      resp.put("message", "查詢成功！");
      resp.put("result", result);
    } else if (id.isEmpty() && accountId.isEmpty() && !kitchenId.isEmpty()) {
      JSONObject result = rh.getByKitchenId(accountId);

      /** 填入回傳訊息與Query結果 */
      resp.put("message", "查詢成功！");
      resp.put("result", result);
    } else {
      resp.put("errors", "查詢條件錯誤！");
    }

    /** 將回傳的資料封裝成response回傳 */
    jsr.response(resp, response);
  }

  /**
   * 處理Http Post method<br>
   * 新增租用資訊(新增廚房)
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    /** 解析傳入的request */
    JsonReader jsr = new JsonReader(request);
    /** 將request中的資料轉成json */
    JSONObject jso = jsr.getObject();
    /** 回傳的json */
    JSONObject resp = new JSONObject();

    /** 取得request中的參數 */
    int accountId = ah.getLoginId(request);
    int kitchenId = jso.getInt("kitchenId");
    Date startAt = new Date(jso.getLong("startAt"));
    Date endAt = new Date(jso.getLong("endAt"));
    int price = jso.getInt("price");

    /** 建立Rent model以進行後續新增至資料庫的動作 */
    Rent rent = new Rent(accountId, kitchenId, startAt, endAt, price);

    /** 根據檢查重複的結果進行新增資料，或是提供錯誤訊息 */
    if (!rh.checkDuplicate(rent)) {
      /** 新增帳號資料 */
      boolean result = rh.create(rent);

      /** 根據新增資料的結果提供成功/失敗訊息 */
      if (result) {
        resp.put("message", "新增成功！");
      } else {
        resp.put("errors", "新增失敗！");
      }
    } else {
      resp.put("errors", "此時段已被租用！");
    }

    /** 將回傳的資料封裝成response回傳 */
    jsr.response(resp, response);
  }

  /**
   * 處理Http Put method<br>
   * 修改租用資訊(編輯租用)
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  public void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    /** 解析傳入的request */
    JsonReader jsr = new JsonReader(request);
    /** 將傳入的request轉成json */
    JSONObject jso = jsr.getObject();
    /** 回傳的json */
    JSONObject resp = new JSONObject();

    /** 取得request中的參數 */
    int id = jso.getInt("id");
    int accountId = jso.getInt("accountId");
    int kitchenId = jso.getInt("kitchenId");
    Date startAt = new Date(jso.getLong("startAt"));
    Date endAt = new Date(jso.getLong("endAt"));
    int price = jso.getInt("price");

    /** 建立Rent model以修改資料 */
    Rent rent = new Rent(id, accountId, kitchenId, startAt, endAt, price);

    /** 根據檢查重複的結果進行更新，或提供錯誤訊息 */
    if (!rh.checkDuplicate(rent)) {
      /** 更新帳號資料 */
      boolean result = rh.update(rent);

      /** 根據更新結果提供成功/失敗訊息 */
      if (result) {
        resp.put("message", "編輯成功！");
      } else {
        resp.put("errors", "編輯失敗！");
      }
    } else {
      resp.put("errors", "此卡號已被登記！");
    }

    /** 將回傳的json封裝成response */
    jsr.response(resp, response);
  }

  /**
   * 處理Http Delete method<br>
   * 刪除租用
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  public void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    /** 解析傳入的request */
    JsonReader jsr = new JsonReader(request);
    /** 將傳入的request轉成json */
    JSONObject jso = jsr.getObject();
    /** 欲刪除的帳號編號 */
    String id = jso.getString("id");
    /** 回傳的json */
    JSONObject resp = new JSONObject();

    /** 根據刪除的結果進行更新，或提供錯誤訊息 */
    if (rh.deleteById(id)) {
      resp.put("message", "刪除成功！");
    } else {
      resp.put("errors", "刪除失敗！");
    }

    /** 將回傳的json封裝成response */
    jsr.response(resp, response);
  }
}
