package controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import org.json.JSONObject;

import app.*;
import tools.JsonReader;

public class AuthController extends HttpServlet {
  /** 版本號，避免版本不相容而導致error */
  private static final long serialVersionUID = 1L;
  private AccountHelper ah = AccountHelper.getHelper();

  /**
   * 處理 Http Get method<br>
   * 取得登入資訊(所有頁面)
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    /** 解析傳入的request */
    JsonReader jsr = new JsonReader(request);
    /** 回傳的json */
    JSONObject resp = new JSONObject();

    /** 存入登入session部分資料 */
    resp.put("message", "查詢成功！");
    resp.put("id", ah.getLoginId(request));
    resp.put("admin", ah.checkAdmin(request));

    /** 將回傳的資料封裝入response回傳 */
    jsr.response(resp, response);
  }

  /**
   * 處理 Http Post method<br>
   * 處理登入申請
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    /** 解析傳入的request */
    JsonReader jsr = new JsonReader(request);
    /** 將request中的資料轉成json */
    JSONObject jso = jsr.getObject();
    /** 回傳的json */
    JSONObject resp = new JSONObject();

    /** 取得request中的參數 */
    String email = jso.getString("email");
    String password = jso.getString("password");

    /** 建立Account model以進行後續存取資料庫的動作 */
    Account account = new Account(email, password);

    /** 根據登入結果提供不同訊息 */
    if (ah.loginAttempt(account, request)) {
      resp.put("message", "登入成功！");
      resp.put("redirect", "/co_kitchen");
    } else {
      resp.put("errors", "帳號密碼錯誤！");
    }

    /** 將回傳的資料封裝入response回傳 */
    jsr.response(resp, response);
  }

  /**
   * 處理 Http Delete method<br>
   * 處理登出請求
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  public void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    /** 解析傳入的request */
    JsonReader jsr = new JsonReader(request);
    /** 回傳的json */
    JSONObject resp = new JSONObject();

    /** 刪除登入session */
    request.getSession().invalidate();

    /** 提供登出訊息 */
    resp.put("message", "登出成功！");

    /** 將回傳的資料封裝入response回傳 */
    jsr.response(resp, response);
  }
}
