package controllers;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import org.json.JSONObject;

import app.*;
import tools.JsonReader;

public class VisaController extends HttpServlet {
  /** 版本號，避免版本不相容而導致error */
  private static final long serialVersionUID = 1L;
  private VisaHelper vh = VisaHelper.getHelper();
  private AccountHelper ah = AccountHelper.getHelper();

  /**
   * 處理Http Get method<br>
   * 1. 取得所有信用卡資料(所有信用卡清單)<br>
   * 2. 取得部分信用卡資料(使用者資訊)<br>
   * 3. 取得信用卡資訊(編輯信用卡)
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    /** 解析傳入的request */
    JsonReader jsr = new JsonReader(request);
    /** 回傳的json */
    JSONObject resp = new JSONObject();
    /** 取得要求的帳號編號 */
    String AccountId = jsr.getParameter("accountId");
    /** 取得要求的信用卡編號 */
    String id = jsr.getParameter("id");

    /** 若無要求的信用卡編號，回傳所有信用卡資料，反之回傳要求的信用卡資料 */
    if (id.isEmpty() && AccountId.isEmpty()) {
      JSONObject result = vh.getAll();

      /** 填入回傳訊息與Query結果 */
      resp.put("message", "查詢成功！");
      resp.put("result", result);
    } else if (!id.isEmpty() && AccountId.isEmpty()) {
      JSONObject result = vh.getById(id);

      /** 填入回傳訊息與Query結果 */
      resp.put("message", "查詢成功！");
      resp.put("result", result);
    } else if (id.isEmpty() && !AccountId.isEmpty()) {
      JSONObject result = vh.getByAccountId(AccountId);

      /** 填入回傳訊息與Query結果 */
      resp.put("message", "查詢成功！");
      resp.put("result", result);
    } else {
      resp.put("errors", "查詢條件錯誤！");
    }

    /** 將回傳的資料封裝成response回傳 */
    jsr.response(resp, response);
  }

  /**
   * 處理Http Post method<br>
   * 新增信用卡資訊(新增信用卡)
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    /** 解析傳入的request */
    JsonReader jsr = new JsonReader(request);
    /** 將request中的資料轉成json */
    JSONObject jso = jsr.getObject();
    /** 回傳的json */
    JSONObject resp = new JSONObject();

    /** 取得request中的參數 */
    int accountId = ah.getLoginId(request);
    String bankId = jso.getString("bankId");
    String cardId = jso.getString("cardId");
    String validationNum = jso.getString("validationNum");
    Date goodThru = new Date(jso.getLong("goodThru"));

    /** 建立Visa model以進行後續新增至資料庫的動作 */
    Visa visa = new Visa(accountId, bankId, cardId, validationNum, goodThru);

    /** 根據檢查重複的結果進行新增資料，或是提供錯誤訊息 */
    if (!vh.checkDuplicate(visa)) {
      /** 新增帳號資料 */
      boolean result = vh.create(visa);

      /** 根據新增資料的結果提供成功/失敗訊息 */
      if (result) {
        resp.put("message", "新增成功！");
      } else {
        resp.put("errors", "新增失敗！");
      }
    } else {
      resp.put("errors", "此卡號已被登記！");
    }

    /** 將回傳的資料封裝成response回傳 */
    jsr.response(resp, response);
  }

  /**
   * 處理Http Put method<br>
   * 編輯信用卡資訊(編輯信用卡)
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  public void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    /** 解析傳入的request */
    JsonReader jsr = new JsonReader(request);
    /** 將傳入的request轉成json */
    JSONObject jso = jsr.getObject();
    /** 回傳的json */
    JSONObject resp = new JSONObject();

    /** 取得request中的參數 */
    int id = jso.getInt("id");
    int accountId = jso.getInt("accountId");
    String bankId = jso.getString("bankId");
    String cardId = jso.getString("cardId");
    String validationNum = jso.getString("validationNum");
    Date goodThru = new Date(jso.getLong("goodThru"));

    /** 建立Visa model以修改資料 */
    Visa visa = new Visa(id, accountId, bankId, cardId, validationNum, goodThru);

    /** 根據檢查重複的結果進行更新，或提供錯誤訊息 */
    if (!vh.checkDuplicate(visa)) {
      /** 更新帳號資料 */
      boolean result = vh.update(visa);

      /** 根據更新結果提供成功/失敗訊息 */
      if (result) {
        resp.put("message", "編輯成功！");
      } else {
        resp.put("errors", "編輯失敗！");
      }
    } else {
      resp.put("errors", "此卡號已被登記！");
    }

    /** 將回傳的json封裝成response */
    jsr.response(resp, response);
  }

  /**
   * 處理Http Delete method<br>
   * . 刪除信用卡
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  public void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    /** 解析傳入的request */
    JsonReader jsr = new JsonReader(request);
    /** 將傳入的request轉成json */
    JSONObject jso = jsr.getObject();
    /** 欲刪除的帳號編號 */
    String id = jso.getString("id");
    /** 回傳的json */
    JSONObject resp = new JSONObject();

    /** 根據刪除的結果進行更新，或提供錯誤訊息 */
    if (vh.deleteById(id)) {
      resp.put("message", "刪除成功！");
    } else {
      resp.put("errors", "刪除失敗！");
    }

    /** 將回傳的json封裝成response */
    jsr.response(resp, response);
  }
}
