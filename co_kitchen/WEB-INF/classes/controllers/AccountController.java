package controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import org.json.JSONObject;

import app.*;
import tools.JsonReader;

public class AccountController extends HttpServlet {
  /** 版本號，避免版本不相容而導致error */
  private static final long serialVersionUID = 1L;
  private AccountHelper ah = AccountHelper.getHelper();

  /**
   * 處理Http Get method<br>
   * 1. 取得所有帳號資料(帳號清單、所有帳號清單)<br>
   * 2. 取得個別帳號資料(使用者資訊、編輯使用者資訊、修改密碼)
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    /** 解析傳入的request */
    JsonReader jsr = new JsonReader(request);
    /** 回傳的json */
    JSONObject resp = new JSONObject();

    /** 取得要求的帳號編號 */
    String id = jsr.getParameter("id");
    /** 紀錄查詢後的資料 */
    JSONObject result = null;

    /** 若無要求的帳號編號，回傳所有帳號資料，反之回傳要求的帳號資料 */
    if (id.isEmpty()) {
      result = ah.getAll();

      /** 填入回傳訊息與Query結果 */
      resp.put("message", "查詢成功！");
      resp.put("result", result);
    } else {
      result = ah.getById(id);

      /** 填入回傳訊息與Query結果 */
      resp.put("message", "查詢成功！");
      resp.put("result", result);
    }

    /** 將回傳的資料封裝入response回傳 */
    jsr.response(resp, response);
  }

  /**
   * 處理Http Post method<br>
   * 新增帳號資訊(註冊帳號、新增使用者)
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    /** 解析傳入的request */
    JsonReader jsr = new JsonReader(request);
    /** 將request中的資料轉成json */
    JSONObject jso = jsr.getObject();
    /** 回傳的json */
    JSONObject resp = new JSONObject();

    /** 取得request中的參數 */
    String firstName = jso.getString("firstName");
    String lastName = jso.getString("lastName");
    String email = jso.getString("email");
    String password = jso.getString("password");
    int admin = jso.getInt("admin");

    /** 建立Account model以進行後續新增至資料庫的動作 */
    Account account = new Account(firstName, lastName, email, password, admin);

    /** 根據檢查重複的結果進行新增資料，或是提供錯誤訊息 */
    if (!ah.checkDuplicate(account)) {
      /** 根據新增帳號資料結果進行不同操作 */
      if (ah.create(account)) {
        /** 依據註冊帳號/新增使用者提供不同訊息 */
        if (account.getAdmin() == 0) {
          /** 自動登入 */
          ah.loginAttempt(account, request);

          resp.put("message", "註冊成功！");

        } else {
          resp.put("message", "新增管理者成功！");
        }
      } else {
        /** 依據註冊帳號/新增使用者提供不同訊息 */
        if (account.getAdmin() == 0) {
          resp.put("errors", "註冊失敗！");
        } else {
          resp.put("errors", "新增管理者失敗！");
        }
      }
    } else {
      /** 依據註冊帳號/新增使用者提供不同訊息 */
      if (account.getAdmin() == 0) {
        resp.put("errors", "此Email帳號已被註冊！");
      } else {
        resp.put("errors", "此Email帳號已被註冊或是已有此員工編號的帳號！");
      }
    }

    /** 將回傳的資料封裝入response回傳 */
    jsr.response(resp, response);
  }

  /**
   * 處理Http Put method<br>
   * 修改帳號資訊(編輯使用者資料、修改密碼)
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  public void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    /** 解析傳入的request */
    JsonReader jsr = new JsonReader(request);
    /** 將傳入的request轉成json */
    JSONObject jso = jsr.getObject();
    /** 回傳的json */
    JSONObject resp = new JSONObject();

    /** 取得request中的參數 */
    int id = jso.getInt("id");
    String firstName = jso.getString("firstName");
    String lastName = jso.getString("lastName");
    String email = jso.getString("email");
    String password = jso.getString("password");
    int admin = jso.getInt("admin");

    /** 建立Account model以修改資料 */
    Account account = new Account(id, firstName, lastName, email, password, admin);

    /** 根據檢查重複的結果進行更新，或提供錯誤訊息 */
    if (!ah.checkDuplicate(account)) {
      /** 根據更新帳號資料結果提供成功/失敗訊息 */
      if (ah.update(account)) {
        /** 若是更新使用者自己的帳號，進行下列操作 */
        if (ah.getLoginId(request) == account.getId()) {
          /** 根據更新session結果進行不同操作 */
          ah.loginAttempt(account, request);

          resp.put("message", "編輯成功！");
        } else {
          resp.put("message", "編輯成功！");
        }
      } else {
        resp.put("errors", "編輯失敗！");
      }
    } else {
      /** 根據編輯自己的帳號資料/管理員更新他人的帳號資料提供不同訊息 */
      if (account.getAdmin() == 0) {
        resp.put("errors", "此Email帳號已被註冊！");
      } else {
        resp.put("errors", "此Email帳號已被註冊或是已有此員工編號的帳號！");
      }
    }

    /** 將回傳的json封裝入response */
    jsr.response(resp, response);
  }

  /**
   * 處理Http Delete method<br>
   * 刪除帳號
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  public void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    /** 解析傳入的request */
    JsonReader jsr = new JsonReader(request);
    /** 將傳入的request轉成json */
    JSONObject jso = jsr.getObject();
    /** 欲刪除的帳號編號 */
    String id = jso.getString("id");
    /** 回傳的json */
    JSONObject resp = new JSONObject();

    /** 根據刪除的結果進行更新，或提供錯誤訊息 */
    if (ah.deleteById(id)) {
      /** 如果是登入中的使用者刪除自己的帳號則強制登出 */
      if (String.valueOf(ah.getLoginId(request)) == id) {
        request.getSession().invalidate();

        resp.put("message", "刪除成功！");
      } else {
        resp.put("message", "刪除成功！");
      }
    } else {
      resp.put("errors", "刪除失敗！");
    }

    /** 將回傳的json封裝入response */
    jsr.response(resp, response);
  }
}
