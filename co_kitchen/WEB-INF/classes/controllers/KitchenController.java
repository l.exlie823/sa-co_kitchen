package controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import org.json.JSONObject;

import app.*;
import tools.JsonReader;

public class KitchenController extends HttpServlet {
  /** 版本號，避免版本不相容而導致error */
  private static final long serialVersionUID = 1L;
  private KitchenHelper kh = KitchenHelper.getHelper();

  /**
   * 處理Http Get method<br>
   * 1. 取得所有廚房資訊(廚房清單、所有廚房清單、新增租用、編輯租用)<br>
   * 2. 取得個別廚房資訊(編輯租用)
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    /** 解析傳入的request */
    JsonReader jsr = new JsonReader(request);
    /** 回傳的json */
    JSONObject resp = new JSONObject();
    /** 取得要求的廚房編號 */
    String id = jsr.getParameter("id");

    /** 若無要求的廚房編號，回傳所有廚房資料，反之回傳要求的廚房資料 */
    if (id.isEmpty()) {
      JSONObject result = kh.getAll();

      /** 填入回傳訊息與Query結果 */
      resp.put("message", "查詢成功！");
      resp.put("result", result);
    } else {
      JSONObject result = kh.getById(id);

      /** 填入回傳訊息與Query結果 */
      resp.put("message", "查詢成功！");
      resp.put("result", result);
    }

    /** 將回傳的資料封裝成response回傳 */
    jsr.response(resp, response);
  }

  /**
   * 處理Http Post method<br>
   * 新增廚房資訊(新增廚房)
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    /** 解析傳入的request */
    JsonReader jsr = new JsonReader(request);
    /** 將request中的資料轉成json */
    JSONObject jso = jsr.getObject();
    /** 回傳的json */
    JSONObject resp = new JSONObject();

    /** 取得request中的參數 */
    String address = jso.getString("address");
    int price = jso.getInt("price");

    /** 建立Kitchen model以進行後續新增至資料庫的動作 */
    Kitchen kitchen = new Kitchen(address, price);

    /** 根據檢查重複的結果進行新增資料，或是提供錯誤訊息 */
    if (!kh.checkDuplicate(kitchen)) {
      /** 根據新增資料的結果提供成功/失敗訊息 */
      if (kh.create(kitchen)) {
        resp.put("message", "新增成功！");
      } else {
        resp.put("errors", "新增失敗！");
      }
    } else {
      resp.put("errors", "此地址已被登記！");
    }

    /** 將回傳的資料封裝成response回傳 */
    jsr.response(resp, response);
  }

  /**
   * 處理Http Put method<br>
   * 修改廚房資訊(編輯廚房)
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  public void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    /** 解析傳入的request */
    JsonReader jsr = new JsonReader(request);
    /** 將傳入的request轉成json */
    JSONObject jso = jsr.getObject();
    /** 回傳的json */
    JSONObject resp = new JSONObject();

    /** 取得request中的參數 */
    int id = jso.getInt("id");
    String address = jso.getString("address");
    int price = jso.getInt("price");

    /** 建立Kitchen model以修改資料 */
    Kitchen kitchen = new Kitchen(id, address, price);

    /** 根據檢查重複的結果進行更新，或提供錯誤訊息 */
    if (!kh.checkDuplicate(kitchen)) {
      /** 更新帳號資料 */
      boolean result = kh.update(kitchen);

      /** 根據更新結果提供成功/失敗訊息 */
      if (result) {
        resp.put("message", "編輯成功！");
      } else {
        resp.put("errors", "編輯失敗！");
      }
    } else {
      resp.put("errors", "此地址已被登記！");
    }

    /** 將回傳的json封裝成response */
    jsr.response(resp, response);
  }

  /**
   * 處理Http Delete method<br>
   * 刪除廚房
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  public void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    /** 解析傳入的request */
    JsonReader jsr = new JsonReader(request);
    /** 將傳入的request轉成json */
    JSONObject jso = jsr.getObject();
    /** 欲刪除的帳號編號 */
    String id = jso.getString("id");
    /** 回傳的json */
    JSONObject resp = new JSONObject();

    /** 根據刪除的結果進行更新，或提供錯誤訊息 */
    if (kh.deleteById(id)) {
      resp.put("message", "刪除成功！");
    } else {
      resp.put("errors", "刪除失敗！");
    }

    /** 將回傳的json封裝成response */
    jsr.response(resp, response);
  }
}
