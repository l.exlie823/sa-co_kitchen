package controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import org.json.JSONObject;

import app.*;
import tools.JsonReader;

public class TransactionController extends HttpServlet {
  /** 版本號，避免版本不相容而導致error */
  private static final long serialVersionUID = 1L;
  private TransactionHelper th = TransactionHelper.getHelper();

  /**
   * 處理Http Get method<br>
   * 取得交易資訊(交易紀錄、所有交易資訊)
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    /** 解析傳入的request */
    JsonReader jsr = new JsonReader(request);
    /** 回傳的json */
    JSONObject resp = new JSONObject();
    /** 取得要求的帳號編號 */
    String AccountId = jsr.getParameter("accountId");
    /** 取得要求的交易編號 */
    String id = jsr.getParameter("id");

    /** 若無要求的交易編號，回傳所有交易資料，反之回傳要求的交易資料 */
    if (id.isEmpty() && AccountId.isEmpty()) {
      JSONObject result = th.getAll();

      /** 填入回傳訊息與Query結果 */
      resp.put("message", "查詢成功！");
      resp.put("result", result);
    } else if (!id.isEmpty() && AccountId.isEmpty()) {
      JSONObject result = th.getById(id);

      /** 填入回傳訊息與Query結果 */
      resp.put("message", "查詢成功！");
      resp.put("result", result);
    } else if (id.isEmpty() && !AccountId.isEmpty()) {
      JSONObject result = th.getByAccountId(AccountId);

      /** 填入回傳訊息與Query結果 */
      resp.put("message", "查詢成功！");
      resp.put("result", result);
    } else {
      resp.put("errors", "查詢條件錯誤！");
    }

    /** 將回傳的資料封裝成response回傳 */
    jsr.response(resp, response);
  }

  /**
   * 處理Http Post method<br>
   * 新增交易資訊(結清款項)
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    /** 解析傳入的request */
    JsonReader jsr = new JsonReader(request);
    /** 將request中的資料轉成json */
    JSONObject jso = jsr.getObject();
    /** 回傳的json */
    JSONObject resp = new JSONObject();

    /** 取得request中的參數 */
    int accountId = jso.getInt("accountId");
    int rentId = jso.getInt("rentId");
    int visaId = jso.getInt("visaId");

    /** 建立Rent model以進行後續新增至資料庫的動作 */
    Transaction transaction = new Transaction(accountId, rentId, visaId);

    /** 根據檢查重複的結果進行新增資料，或是提供錯誤訊息 */
    if (!th.checkDuplicate(transaction)) {
      /** 新增交易資料 */
      boolean result = th.create(transaction);

      /** 根據新增資料的結果提供成功/失敗訊息 */
      if (result) {
        resp.put("message", "交易成功！");
      } else {
        resp.put("errors", "交易失敗！");
      }
    } else {
      resp.put("errors", "此租用已結清款項！");
    }

    /** 將回傳的資料封裝成response回傳 */
    jsr.response(resp, response);
  }
}
